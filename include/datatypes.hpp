/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

namespace sgp {

enum class F_PHYS
{
   SIMPLIFIED, // F_phys = J - (D * dJ/dD * D) : inv(D)
   GENERALIZED // F_phys = J - ε ⋅ [ΔD inv(I + Γ ΔD) ε]
};

enum class F_REG
{
   ANGLE,  // F_reg = 1/2||(I-F)φ||^2
   SIN_COS // F_reg = 1/2(||(I-F)sin(2φ)||^2 + ||(I-F)cos(2φ)||^2)
};

} // namespace sgp
