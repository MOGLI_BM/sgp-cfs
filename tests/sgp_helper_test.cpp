/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "random.hpp"
#include "test_implementation.hpp"

using namespace sgp;
using namespace test;

CSR_Matrix create_random_matrix(int rows, int cols, int nonzerosperrow, double minval, double maxval, int seed)
{
   std::vector<double> values(nonzerosperrow * rows);
   std::vector<int>    col_idx(nonzerosperrow * rows);
   std::vector<int>    row_idx(rows + 1);

   double density = double(nonzerosperrow) / double(cols);

   Random random(0, 1, seed);
   Random randomVal(minval, maxval, seed);

   int count = 0;
   int i, j;

   row_idx[0] = 0;

   for (i = 0; i < rows; ++i)
   {
      int attempts = 0;

      while (1)
      {
         count = 0;

         for (j = 0; j < cols; ++j)
         {
            if (count == nonzerosperrow)
            {
               break;
            }

            if (random.real() <= density)
            {
               col_idx[i * nonzerosperrow + count] = j;
               values[i * nonzerosperrow + count]  = randomVal.real();

               ++count;
            }
         }

         row_idx[i + 1] = row_idx[i] + count;

         ++attempts;

         if (count == nonzerosperrow)
         {
            break;
         }
      }
   }

   CSR_Matrix M(rows, cols, row_idx.data(), col_idx.data(), values.data());
   return M;
}

void dAF_test(SGP_Testclass& algo, const CSR_Matrix& F)
{
   auto dAF = algo.get_dAF(F);
   std::cout << dAF.D << std::endl;
   // auto        err = diff(Q, id);
   // std::string msg = "Q(R(0,0,0)) != id";
   // check_almost_zero(err, msg);
}

int main(int argc, char const* argv[])
{
   // random engine
   // const int n_cases = 50;
   // Random       random(-10, 10, 17);

   const int n_el = 50;

   // filter matrix
   auto F = create_random_matrix(n_el, n_el, 10, -10, 10, 17);


   // run tests
   for (int dim = 2; dim <= 3; ++dim)
   {
      // test implementation
      auto initparams = sample_params_init(dim, n_el);
      SGP_Testclass algorithm(initparams);

      // dAF
      dAF_test(algorithm, F);
   }

   return 0;
}
