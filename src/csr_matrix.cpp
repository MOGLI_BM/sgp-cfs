/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "csr_matrix.hpp"

#include <cassert>
#include <cstring>
#include <iostream>
#include <numeric>
#include <vector>

namespace sgp {

CSR_Matrix CSR_Matrix::diag(const std::vector<double>& diag_val)
{
   const int n = diag_val.size();

   std::vector<MKL_INT> rows(n + 1);
   std::iota(rows.begin(), rows.end(), 0);
   auto cols = rows;

   return CSR_Matrix(n, n, rows.data(), cols.data(), diag_val.data());
}

CSR_Matrix CSR_Matrix::id(int n)
{
   return diag(std::vector<double>(n, 1.0));
}

CSR_Matrix::CSR_Matrix(int n)
: _mkl_handle(0)
, _rows(0)
, _cols(0)
{
   if (n > 0)
   {
      *this = id(n);
   }
}

template <typename INT_T>
CSR_Matrix::CSR_Matrix(int           rows,
                       int           cols,
                       const INT_T*  row_start_idx,
                       const INT_T*  row_end_idx,
                       const INT_T*  col_idx,
                       const double* values)
: _mkl_handle(0)
, _rows(rows)
, _cols(cols)
{
   // create temporary arrays
   int                  nonZ = row_end_idx[rows - 1];
   std::vector<MKL_INT> mkl_rows_start(rows);
   std::vector<MKL_INT> mkl_rows_end(rows);
   std::vector<MKL_INT> mkl_col_idx(nonZ);
   std::vector<double>  mkl_values(nonZ);

   // copy input data
   for (int i = 0; i < rows; ++i)
   {
      mkl_rows_start[i] = row_start_idx[i];
      mkl_rows_end[i]   = row_end_idx[i];
   }

   for (int i = 0; i < nonZ; ++i)
   {
      mkl_col_idx[i] = col_idx[i];
      mkl_values[i]  = values[i];
   }

   // create sparse-matrix-handle from temporary arrays
   sparse_matrix_t mkl_tmp_handle;
   mkl_sparse_d_create_csr(&mkl_tmp_handle,
                           SPARSE_INDEX_BASE_ZERO,
                           _rows,
                           _cols,
                           mkl_rows_start.data(),
                           mkl_rows_end.data(),
                           mkl_col_idx.data(),
                           mkl_values.data());

   // create copy of handle, s.th. data is handled by mkl internally
   copy_mkl_sparse_matrix(mkl_tmp_handle);
   mkl_sparse_destroy(mkl_tmp_handle);

   get_data_from_handle();
}

template <typename INT_T>
CSR_Matrix::CSR_Matrix(int rows, int cols, const INT_T* row_idx, const INT_T* col_idx, const double* values)
: CSR_Matrix(rows, cols, row_idx, row_idx + 1, col_idx, values)
{}

CSR_Matrix::CSR_Matrix(sparse_matrix_t mkl_handle, bool copy)
: _mkl_handle(0)
{
   if (copy)
   {
      copy_mkl_sparse_matrix(mkl_handle);
   }
   else
   {
      _mkl_handle = mkl_handle;
   }

   get_data_from_handle();
}

CSR_Matrix::CSR_Matrix(const CSR_Matrix& other)
: CSR_Matrix(other._mkl_handle, true)
{}

CSR_Matrix::CSR_Matrix(CSR_Matrix&& other)
: CSR_Matrix(other._mkl_handle, false)
{
   other._mkl_handle = 0;
}

CSR_Matrix::~CSR_Matrix()
{
   if (_mkl_handle != 0)
   {
      mkl_sparse_destroy(_mkl_handle);
   }
}

CSR_Matrix& CSR_Matrix::operator=(const CSR_Matrix& other)
{
   assign(other._mkl_handle, true);
   return *this;
}

CSR_Matrix& CSR_Matrix::operator=(CSR_Matrix&& other)
{
   assign(other._mkl_handle, false);
   other._mkl_handle = 0;
   return *this;
}

CSR_Matrix CSR_Matrix::operator*(const CSR_Matrix& B) const
{
   sparse_matrix_t C;
   mkl_sparse_spmm(SPARSE_OPERATION_NON_TRANSPOSE, this->_mkl_handle, B._mkl_handle, &C);
   return CSR_Matrix(C, false);
}

CSR_Matrix CSR_Matrix::operator-(const CSR_Matrix& B) const
{
   sparse_matrix_t C;
   // compute -1*B + A
   mkl_sparse_d_add(SPARSE_OPERATION_NON_TRANSPOSE, B._mkl_handle, -1, this->_mkl_handle, &C);

   return CSR_Matrix(C, false);
}

CSR_Matrix CSR_Matrix::operator+(const CSR_Matrix& B) const
{
   sparse_matrix_t C;
   mkl_sparse_d_add(SPARSE_OPERATION_NON_TRANSPOSE, B._mkl_handle, 1, this->_mkl_handle, &C);

   return CSR_Matrix(C, false);
}

double CSR_Matrix::operator()(const int i, const int j) const
{
   for (int jj = int(_row_start[i]); jj < int(_row_end[i]); ++jj)
   {
      if (j == int(_col_idx[jj]))
      {
         return _values[jj];
      }
   }

   return 0.0;
}

Vector& CSR_Matrix::apply(const Vector& x, Vector& y, double alpha, double beta) const
{
   assert(x.size() == size_t(_cols));
   assert(y.size() == size_t(_rows));
   mkl_sparse_d_mv(SPARSE_OPERATION_NON_TRANSPOSE,
                   alpha,
                   _mkl_handle,
                   {SPARSE_MATRIX_TYPE_GENERAL, SPARSE_FILL_MODE_LOWER, SPARSE_DIAG_NON_UNIT},
                   x.data(),
                   beta,
                   y.data());
   return y;
}

CSR_Matrix CSR_Matrix::transpose() const
{
   sparse_matrix_t C;
   CSR_Matrix      I(this->_rows);
   mkl_sparse_spmm(SPARSE_OPERATION_TRANSPOSE, this->_mkl_handle, I._mkl_handle, &C);
   return CSR_Matrix(C, false);
}

CSR_Matrix CSR_Matrix::AtA() const
{
   sparse_matrix_t C;
   mkl_sparse_spmm(SPARSE_OPERATION_TRANSPOSE, this->_mkl_handle, this->_mkl_handle, &C);
   // mkl_sparse_syrk(SPARSE_OPERATION_TRANSPOSE, this->_mkl_handle, &C);
   return CSR_Matrix(C, false);
}

Vector CSR_Matrix::diag() const
{
   assert(_rows == _cols);
   Vector d(_rows, 1.0);
   mkl_sparse_d_mv(SPARSE_OPERATION_NON_TRANSPOSE,
                   1.0,
                   _mkl_handle,
                   {SPARSE_MATRIX_TYPE_DIAGONAL, SPARSE_FILL_MODE_LOWER, SPARSE_DIAG_NON_UNIT},
                   d.data(),
                   0.0,
                   d.data());
   return d;
}

double CSR_Matrix::normF() const
{
   return cblas_dnrm2(nonzeros(), _values, 1);
}

void CSR_Matrix::copy_mkl_sparse_matrix(const sparse_matrix_t mkl_handle)
{
   mkl_sparse_copy(mkl_handle, {SPARSE_MATRIX_TYPE_GENERAL, SPARSE_FILL_MODE_LOWER, SPARSE_DIAG_NON_UNIT}, &_mkl_handle);
}

void CSR_Matrix::get_data_from_handle()
{
   sparse_index_base_t indexing;
   mkl_sparse_d_export_csr(_mkl_handle, &indexing, &_rows, &_cols, &_row_start, &_row_end, &_col_idx, &_values);
}

void CSR_Matrix::assign(sparse_matrix_t mkl_handle, bool copy)
{
   if (_mkl_handle != 0)
   {
      mkl_sparse_destroy(_mkl_handle);
   }

   if (copy)
   {
      copy_mkl_sparse_matrix(mkl_handle);
   }
   else
   {
      _mkl_handle = mkl_handle;
   }

   get_data_from_handle();
}

// create output in dense matrix format
std::ostream& operator<<(std::ostream& out, const CSR_Matrix& M)
{
   for (int i = 0; i < M.rows(); ++i)
   {
      for (int j = 0; j < M.cols(); ++j)
      {
         out << M(i, j) << " ";
      }

      out << "\n";
   }

   return out;
}

template CSR_Matrix::CSR_Matrix<int32_t>(int, int, const int32_t*, const int32_t*, const double*);
template CSR_Matrix::CSR_Matrix<int32_t>(int, int, const int32_t*, const int32_t*, const int32_t*, const double*);
template CSR_Matrix::CSR_Matrix<int64_t>(int, int, const int64_t*, const int64_t*, const double*);
template CSR_Matrix::CSR_Matrix<int64_t>(int, int, const int64_t*, const int64_t*, const int64_t*, const double*);
template CSR_Matrix::CSR_Matrix<uint32_t>(int, int, const uint32_t*, const uint32_t*, const double*);
template CSR_Matrix::CSR_Matrix<uint32_t>(int, int, const uint32_t*, const uint32_t*, const uint32_t*, const double*);
template CSR_Matrix::CSR_Matrix<uint64_t>(int, int, const uint64_t*, const uint64_t*, const double*);
template CSR_Matrix::CSR_Matrix<uint64_t>(int, int, const uint64_t*, const uint64_t*, const uint64_t*, const double*);

} // namespace sgp
