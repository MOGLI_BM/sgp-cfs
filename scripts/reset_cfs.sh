#!/bin/bash

# script used internally during development phase

cd ../..

if [[ ${1} == reset_sgp_lib ]]; then
    echo REZIPPING SGP LIB ...
    sleep 10

    zip -r cfsdepscache/sources/sgp/sgp.zip sgp_cfs

    rm cfsdepscache/precompiled/sgp_*
    rm -rf CFS/build/cfsdeps/sgp
else
    echo USING OLD SGP LIB FOR CFS ...
    sleep 10
fi

cd CFS/build

if [[ ${1} == reset_sgp_lib ]]; then
    cmake ..
fi

make -j