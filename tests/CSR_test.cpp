/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include <iostream>

#include "csr_matrix.hpp"
#include "random.hpp"
#include "testing.hpp"
#include "vector.hpp"

using namespace sgp;
using namespace test;

CSR_Matrix create_random_matrix(int rows, int cols, int nonzerosperrow, Random& rand_minmax, Random& rand_01)
{
   std::vector<double> values(nonzerosperrow * rows);
   std::vector<int>    col_idx(nonzerosperrow * rows);
   std::vector<int>    row_idx(rows + 1);

   double density = double(nonzerosperrow) / double(cols);

   int count = 0;
   int i, j;

   row_idx[0] = 0;

   for (i = 0; i < rows; ++i)
   {
      int attempts = 0;

      while (1)
      {
         count = 0;

         for (j = 0; j < cols; ++j)
         {
            if (count == nonzerosperrow)
            {
               break;
            }

            if (rand_01.real() <= density)
            {
               col_idx[i * nonzerosperrow + count] = j;
               values[i * nonzerosperrow + count]  = rand_minmax.real();

               ++count;
            }
         }

         row_idx[i + 1] = row_idx[i] + count;

         ++attempts;

         if (count == nonzerosperrow)
         {
            break;
         }
      }
   }

   CSR_Matrix M(rows, cols, row_idx.data(), col_idx.data(), values.data());
   return M;
}

Vector create_random_vector(int n, Random& random)
{
   Vector vec(n);

   for (int i = 0; i < n; ++i)
   {
      vec[i] = random.real();
   }

   return vec;
}

void CSR_zero_test()
{
   std::cout << "TEST: CSR_Matrix::CSR_Matrix(0)\n";
   CSR_Matrix M;

   if (M.rows() || M.cols())
   {
      std::cerr << "ERROR: M is not a 0x0 matrix!" << std::endl;
      exit(1);
   }
}

void CSR_id_test(Vector& x)
{
   int n = x.size();

   std::cout << "TEST: CSR_Matrix::id(" << n << ")\n";

   auto id = CSR_Matrix::id(n); // id

   Vector y(n); // id*x
   id.apply(x, y);

   auto        err = diff(y, x);
   std::string msg = "id*x != x";
   check_almost_zero(err, msg);
}

void CSR_diag_test(Vector& x)
{
   std::cout << "TEST: CSR_Matrix::diag(x)\n";

   auto   D = CSR_Matrix::diag(x); // diag(x)
   Vector ones(x.size(), 1.0);     // 1
   Vector y(x.size());             // D*1

   D.apply(ones, y);

   auto        err = diff(y, x);
   std::string msg = "diag(x)*1 != x";
   check_almost_zero(err, msg);
}

void CSR_add_test(const CSR_Matrix& A, const CSR_Matrix& B, const Vector& x)
{
   std::cout << "TEST: CSR_Matrix::operator+(CSR_Matrix)\n";

   // add matrices
   auto AplusB  = A + B; // A+B
   auto AminusB = A - B; // A-B

   // initialize solution vectors
   Vector AplusBx(x.size());  // (A+B)x
   Vector AminusBx(x.size()); // (A-B)x
   Vector Ax(x.size());       // A*x
   Vector Bx(x.size());       // B*x

   // apply matrices
   A.apply(x, Ax);
   B.apply(x, Bx);
   AplusB.apply(x, AplusBx);
   AminusB.apply(x, AminusBx);

   // check error
   Vector      err;
   std::string msg;

   err = diff(sum(Ax, Bx), AplusBx);
   msg = "Ax+Bx != (A+B)x";
   check_almost_zero(err, msg, 1e-12);

   err = diff(diff(Ax, Bx), AminusBx);
   msg = "Ax-Bx != (A-B)x";
   check_almost_zero(err, msg, 1e-12);
}

void CSR_LDR_test(const CSR_Matrix& A)
{
   std::cout << "TEST: LDR_Matrix::LDR_Matrix(A)\n";

   LDR_Matrix LDR(A); // L+D+R

   auto D = CSR_Matrix::diag(LDR.D);

   auto        err = A - D - LDR.LR;
   std::string msg = "A-D != L+R";
   check_almost_zero(err.normF(), msg);
}

void CSR_mul_test(const CSR_Matrix& A, const CSR_Matrix& B, const Vector& x)
{
   std::cout << "TEST: CSR_Matrix::operator*(B)\n";

   Vector Bx(x.size());   // B*x
   Vector A_Bx(x.size()); // A*(B*x)
   Vector AB_x(x.size()); // (A*B)*x

   B.apply(x, Bx);
   A.apply(Bx, A_Bx);
   (A * B).apply(x, AB_x);

   auto        err = diff(AB_x, A_Bx);
   std::string msg = "(A*B)*x != A*(B*x)";
   check_almost_zero(err, msg, 1e-11);
}

void CSR_AtA_test(const CSR_Matrix& A)
{
   std::cout << "TEST: CSR_Matrix::AtA()\n";

   auto At = A.transpose(); // A^t
   auto AtA = A.AtA(); // (A^t * A)

   auto        err = (At*A) - AtA;
   std::string msg = "At*A != A.AtA()";
   check_almost_zero(err.normF(), msg, 1e-11);
}

int main(int argc, char const* argv[])
{
   // random devices
   const int seed = 174;
   Random    rnd_01(0, 1, seed);
   Random    rnd_symrange10(-10, 10, seed);

   // run test
   CSR_zero_test();

   for (int n = 10; n < 300; n *= 2)
   {
      std::cout << "\ntesting for " << n << " degrees of freedom:\n";

      // initialize random matrices and vectors
      auto x = create_random_vector(n, rnd_symrange10);
      auto A = create_random_matrix(n, n, 10, rnd_symrange10, rnd_01);
      auto B = create_random_matrix(n, n, 10, rnd_symrange10, rnd_01);

      // id
      CSR_id_test(x);

      // diag
      CSR_diag_test(x);

      // sum/diff
      CSR_add_test(A, B, x);

      // A*B
      CSR_mul_test(A, B, x);

      // split A=L+D+R
      CSR_LDR_test(A);

      // A.AtA()
      CSR_AtA_test(A);
   }

   return 0;
}
