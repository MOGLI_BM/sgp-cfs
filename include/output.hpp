/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <fstream>
#include <iostream>

#include "datatypes.hpp"
#include "format.hpp"
#include "status.hpp"
#include "timer.hpp"
#include "vector.hpp"

namespace sgp {

class Output
{
 public:
   Output(bool print, const std::string& filename)
   : _print(print)
   , __fstream(filename)
   , _stream((filename.empty()) ? std::cout : __fstream)
   , _old_coutflags(std::cout.flags())
   , _old_coutprec(std::cout.precision())
   , _autoCol(12, 6)
   , _sciCol(12, 3)
   , _key(20, 0, Format::LEFT)
   , _var(8, 0, Format::RIGHT)
   , _val(8, 6)
   , _prt(5, 3, Format::FIXED)
   , _iter(0)
   {
      if (print)
      {
         _stream << _autoCol << "Iteration" << sep //
                 << _autoCol << "J" << sep         //
                 << _autoCol << "F_merit" << sep   //
                 << _autoCol << "F_phys" << sep    //
                 << _autoCol << "F_vol" << sep     //
                 << _autoCol << "F_reg" << sep     //
                 << _autoCol << "tau" << sep       //
                 << _autoCol << "lambda" << sep    //
                 << _autoCol << "vol" << sep       //
                 << _autoCol << "J_old - J" << sep //
                 << _autoCol << "n_subprb" << sep  //
                 << _autoCol << "time" << std::endl;
      }
   }

   ~Output()
   {
      // restore outstream to previous state
      _stream.flags(_old_coutflags);
      _stream.precision(_old_coutprec);
   }

   void iterinfo(int    iter,
                 double J,
                 double F_merit,
                 double F_phys,
                 double F_vol,
                 double F_reg,
                 double tau,
                 double lambda,
                 double vol,
                 double conv_val,
                 int    n_subproblems,
                 double time)
   {
      if (_print && iter != _iter)
      {
         _iter = iter; // prevent repeated output of same iteration

         _stream << _autoCol << iter << sep          //
                 << _autoCol << J << sep             //
                 << _autoCol << F_merit << sep       //
                 << _autoCol << F_phys << sep        //
                 << _autoCol << F_vol << sep         //
                 << _autoCol << F_reg << sep         //
                 << _autoCol << tau << sep           //
                 << _autoCol << lambda << sep        //
                 << _autoCol << vol << sep           //
                 << _autoCol << conv_val << sep      //
                 << _autoCol << n_subproblems << sep //
                 << _autoCol << time << std::endl;
      }
   }

   void warning(const std::string& message)
   {
      _stream << "WARNING: " << message << "\n";
   }

   void statusinfo(const Info& info)
   {
      if (_print)
      {
         _stream << "\n";
         switch (info.status)
         {
            case Status::CONVERGED:
               _stream << "Algorithm converged after " << info.n_iterations << " iterations.\n";
               break;
            case Status::MAX_ITERATIONS:
               warning("Algorithm not converged. Maximum number of iterations reached!");
               break;
            case Status::MAX_BISECTIONS:
               warning("Algorithm not converged. Volume bound could not be satisfied!");
               break;
            case Status::ABORTED_BY_USER:
               warning("Algorithm not converged. Aborted by user!");
               break;
            default:
               break;
         }
      }
   }

   void timinginfo(const Vector& time)
   {
      if (_print)
      {
         _stream << "Computation time:\n";
         _stream << _key << "total time:" //
                 << _var << "T = " << _val << time[Timer::TOTAL] << " s\n";
         _stream << _key << "eval_J():"                                        //
                 << _var << "t_J = " << _val << time[Timer::EVAL_J] << " s = " //
                 << _prt << time[Timer::EVAL_J] / time[Timer::TOTAL] << " T\n";
         _stream << _key << "eval_dJ():"                                         //
                 << _var << "t_dJ = " << _val << time[Timer::EVAL_DJ] << " s = " //
                 << _prt << time[Timer::EVAL_DJ] / time[Timer::TOTAL] << " T\n";
         _stream << _key << "solve subproblems:"                                               //
                 << _var << "t_s = " << _val << time[Timer::SOLVE_SUBPROBLEM_TOTAL] << " s = " //
                 << _prt << time[Timer::SOLVE_SUBPROBLEM_TOTAL] / time[Timer::TOTAL] << " T\n";

         // _stream << _key << " -- internal compute sin,cos:" << _var << "t_sincos = " << _val << time[EVAL_SINCOS]
         //           << " s = " << _prt << time[EVAL_SINCOS] / time[SOLVE_SUBPROBLEM_TOTAL] << " t_s\n";
         // _stream << _key << " -- internal compute inv(D):" << _var << "t_D = " << _val << time[EVAL_D] << " s = " << _prt
         //           << time[EVAL_D] / time[SOLVE_SUBPROBLEM_TOTAL] << " t_s\n";
         // ;
         // _stream << _key << " -- internal compute F_merit:" << _var << "t_F = " << _val << time[EVAL_F] << " s = " << _prt
         //           << time[EVAL_F] / time[SOLVE_SUBPROBLEM_TOTAL] << " t_s\n";
         // ;
      }
   }

 private:
   static constexpr const char* sep = " |";
   const bool                   _print;
   std::ofstream                __fstream;
   std::ostream&                _stream;

   const std::ios_base::fmtflags _old_coutflags;
   const std::streamsize         _old_coutprec;

   const Format _autoCol, _sciCol, _key, _var, _val, _prt;

   int _iter;
};

} // namespace sgp