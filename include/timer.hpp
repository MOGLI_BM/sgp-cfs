/*
   Measure execution times for performance comparison

   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <assert.h>
#include <chrono>
#include <map>

namespace sgp {

class Timer
{
 public:
   enum Sections
   {
      TOTAL,
      EVAL_J,
      EVAL_DJ,
      SOLVE_SUBPROBLEM_TOTAL,
      INIT_SUB,
      EVAL_SINCOS,
      EVAL_D,
      EVAL_F,
      TIMER,
      N_TIMER_SECTIONS
   };

   Timer()
   : t_total(std::chrono::duration<double>::zero())
   , t_section(std::chrono::duration<double>::zero())
   , running(false)
   {}

   /* begin new timing section
        @param  resume whether timing shall be resumed immediately
        @returns duration of previous section
    */
   inline double new_section(bool resume = false)
   {
      assert(!(running));

      double d  = time_section();
      t_section = std::chrono::duration<double>::zero();

      if (resume)
         start();

      return d;
   }

   /* start/resume timing
    */
   inline void start()
   {
      assert(!(running));

      running = true;
      t0      = std::chrono::high_resolution_clock::now();
   }

   /* stop/pause timing
        @returns time in seconds since hitting start()
    */
   inline double stop()
   {
      assert(running);

      t_part = std::chrono::high_resolution_clock::now() - t0;
      t_total += t_part;
      t_section += t_part;
      running = false;

      return t_part.count();
   }

   /* get total time
        @return sum of all timing sections in seconds
    */
   inline double time_total() const { return t_total.count(); }

   /* get time of current section
        @return duration of current timing section in seconds
    */
   inline double time_section() const { return t_section.count(); }

 private:
   std::chrono::_V2::system_clock::time_point t0;
   std::chrono::duration<double>              t_total, t_part, t_section;
   bool                                       running;
};

} // namespace sgp
