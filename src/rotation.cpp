/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "rotation.hpp"

namespace sgp {

Matrix rotation_matrix(int dim, const Rotation& cos, const Rotation& sin, bool inverse)
{
   assert(2 <= dim && dim <= 3);

   Matrix R(dim);

   if (dim == 2)
   {
      R(0, 0) = cos[PHI];
      R(1, 1) = cos[PHI];

      if (inverse)
      {
         R(1, 0) = -sin[PHI];
         R(0, 1) = sin[PHI];
      }
      else
      {
         R(0, 1) = -sin[PHI];
         R(1, 0) = sin[PHI];
      }
   }
   else // dim == 3
   {
      R(0, 0) = cos[THETA] * cos[PSI];
      R(1, 1) = cos[PHI] * cos[PSI] - sin[PHI] * sin[THETA] * sin[PSI];
      R(2, 2) = cos[PHI] * cos[THETA];

      if (inverse)
      {
         R(1, 0) = -cos[THETA] * sin[PSI];
         R(2, 0) = sin[THETA];
         R(0, 1) = cos[PHI] * sin[PSI] + sin[PHI] * sin[THETA] * cos[PSI];
         R(2, 1) = -sin[PHI] * cos[THETA];
         R(0, 2) = sin[PHI] * sin[PSI] - cos[PHI] * sin[THETA] * cos[PSI];
         R(1, 2) = sin[PHI] * cos[PSI] + cos[PHI] * sin[THETA] * sin[PSI];
      }
      else
      {
         R(0, 1) = -cos[THETA] * sin[PSI];
         R(0, 2) = sin[THETA];
         R(1, 0) = cos[PHI] * sin[PSI] + sin[PHI] * sin[THETA] * cos[PSI];
         R(1, 2) = -sin[PHI] * cos[THETA];
         R(2, 0) = sin[PHI] * sin[PSI] - cos[PHI] * sin[THETA] * cos[PSI];
         R(2, 1) = sin[PHI] * cos[PSI] + cos[PHI] * sin[THETA] * sin[PSI];
      }
   }

   return R;
}

Matrix convert_to_Voigt(const Matrix& R)
{
   auto dim = R.rows();

   assert(2 <= dim && dim <= 3);

   Matrix Q(n_VoigtNotation(dim));

   if (dim == 2)
   {
      Q(0, 0) = R(0, 0) * R(0, 0);
      Q(0, 1) = R(0, 1) * R(0, 1);
      Q(0, 2) = 2.0 * R(0, 0) * R(0, 1);
      Q(1, 0) = R(1, 0) * R(1, 0);
      Q(1, 1) = R(1, 1) * R(1, 1);
      Q(1, 2) = 2.0 * R(1, 0) * R(1, 1);
      Q(2, 0) = R(0, 0) * R(1, 0);
      Q(2, 1) = R(0, 1) * R(1, 1);
      Q(2, 2) = R(0, 0) * R(1, 1) + R(0, 1) * R(1, 0);
   }
   else // dim == 3
   {
      Q(0, 0) = R(0, 0) * R(0, 0);
      Q(0, 1) = R(0, 1) * R(0, 1);
      Q(0, 2) = R(0, 2) * R(0, 2);
      Q(0, 3) = 2.0 * R(0, 1) * R(0, 2);
      Q(0, 4) = 2.0 * R(0, 0) * R(0, 2);
      Q(0, 5) = 2.0 * R(0, 0) * R(0, 1);

      Q(1, 0) = R(1, 0) * R(1, 0);
      Q(1, 1) = R(1, 1) * R(1, 1);
      Q(1, 2) = R(1, 2) * R(1, 2);
      Q(1, 3) = 2.0 * R(1, 1) * R(1, 2);
      Q(1, 4) = 2.0 * R(1, 0) * R(1, 2);
      Q(1, 5) = 2.0 * R(1, 0) * R(1, 1);

      Q(2, 0) = R(2, 0) * R(2, 0);
      Q(2, 1) = R(2, 1) * R(2, 1);
      Q(2, 2) = R(2, 2) * R(2, 2);
      Q(2, 3) = 2.0 * R(2, 1) * R(2, 2);
      Q(2, 4) = 2.0 * R(2, 0) * R(2, 2);
      Q(2, 5) = 2.0 * R(2, 0) * R(2, 1);

      Q(3, 0) = R(1, 0) * R(2, 0);
      Q(3, 1) = R(1, 1) * R(2, 1);
      Q(3, 2) = R(1, 2) * R(2, 2);
      Q(3, 3) = R(1, 1) * R(2, 2) + R(1, 2) * R(2, 1);
      Q(3, 4) = R(1, 0) * R(2, 2) + R(1, 2) * R(2, 0);
      Q(3, 5) = R(1, 0) * R(2, 1) + R(1, 1) * R(2, 0);

      Q(4, 0) = R(0, 0) * R(2, 0);
      Q(4, 1) = R(0, 1) * R(2, 1);
      Q(4, 2) = R(0, 2) * R(2, 2);
      Q(4, 3) = R(0, 1) * R(2, 2) + R(0, 2) * R(2, 1);
      Q(4, 4) = R(0, 0) * R(2, 2) + R(0, 2) * R(2, 0);
      Q(4, 5) = R(0, 0) * R(2, 1) + R(0, 1) * R(2, 0);

      Q(5, 0) = R(0, 0) * R(1, 0);
      Q(5, 1) = R(0, 1) * R(1, 1);
      Q(5, 2) = R(0, 2) * R(1, 2);
      Q(5, 3) = R(0, 1) * R(1, 2) + R(0, 2) * R(1, 1);
      Q(5, 4) = R(0, 0) * R(1, 2) + R(0, 2) * R(1, 0);
      Q(5, 5) = R(0, 0) * R(1, 1) + R(0, 1) * R(1, 0);
   }

   return Q;
}

} // namespace sgp
