/*
   Base class for sequential global programming (SGP) algorithm.

   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "sgp.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <numeric>
#include <omp.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "csr_matrix.hpp"
#include "datatypes.hpp"
#include "format.hpp"
#include "material_parameters.hpp"
#include "output.hpp"
#include "params.hpp"
#include "rotation.hpp"
#include "tensor.hpp"
#include "timer.hpp"
#include "vector.hpp"

// loop over all design variables
#define LOOP_DESIGN_VAR(DIM, IDX, BODY)              \
   if (DIM == 2)                                     \
   {                                                 \
      for (const auto& IDX : DesignVariables::all2D) \
         BODY                                        \
   }                                                 \
   else                                              \
   {                                                 \
      for (const auto& IDX : DesignVariables::all3D) \
         BODY                                        \
   }

// loop over all angles
#define LOOP_ANGLES(DIM, IDX, BODY)                     \
   if (DIM == 2)                                        \
   {                                                    \
      for (const auto& IDX : DesignVariables::angles2D) \
         BODY                                           \
   }                                                    \
   else                                                 \
   {                                                    \
      for (const auto& IDX : DesignVariables::angles3D) \
         BODY                                           \
   }

// throw runtime error
#define SGP_ERROR(MSG)                     \
   {                                       \
      std::ostringstream msg;              \
      msg << "SGP_ERROR: " << MSG;         \
      throw std::runtime_error(msg.str()); \
   }

#define SGP_CHECK(TEST, MSG) \
   if (!(TEST))              \
   SGP_ERROR(MSG)

namespace sgp {

constexpr double EPSILON = 1e-16;
constexpr double INF     = std::numeric_limits<double>::infinity();

Algorithm::Algorithm(const InitParams& args)
: _dim(args.dim)
// , _phys_model(args.model)
// , _filt_model(args.filt)
// , _n_ip((args.model == F_PHYS::SIMPLIFIED) ? 0 : args.n_ip)
, _n_el(args.v_FE.size())
, _D_core(args.D_core)
, _D_core_inv(args.D_core, true)
, _v_FE(args.v_FE)
, _v_tot(std::accumulate(args.v_FE.begin(), args.v_FE.end(), 0.0))
, _info({Status::NOT_AVAILABLE, 0, INF})
{
   SGP_CHECK(_dim == 2 || _dim == 3, "only 2 and 3 dimensional domains supported!");
   SGP_CHECK(_D_core.rows() == n_VoigtNotation(_dim), "stiffness tensor D does not fit spacial dimension!");
}

Material_Vector Algorithm::solve(const SolveParams& args, double lambda_init)
{
   auto args_cpy = args;

   // === init step ===
   // fix lambda
   args_cpy.lambda_min = lambda_init;
   args_cpy.lambda_max = lambda_init;
   // ignore volume bound
   args_cpy.eps = INF;
   // run init step
   auto x_init = solve(args_cpy, false);
   // check stopping criterion
   assert(_info.status == CONVERGED || _info.status == MAX_ITERATIONS || _info.status == ABORTED_BY_USER);

   // === volume bisection step ===
   // reset args to enable volume bisection
   args_cpy.lambda_min = args.lambda_min;
   args_cpy.lambda_max = args.lambda_max;
   args_cpy.eps        = args.eps;
   // set initial guess to x_init
   args_cpy.rho0 = x_init[RHO];
   args_cpy.phi0 = x_init[PHI];
   if (_dim == 3)
   {
      args_cpy.psi0   = x_init[PSI];
      args_cpy.theta0 = x_init[THETA];
   }
   // run second step
   return solve(args_cpy);
}

Material_Vector Algorithm::solve(const SolveParams& args, bool summary)
{
   // initialize parameters
   _info = Info{Status::RUNNING, 0, INF};

   _phys_model = args.model;
   _filt_model = args.filt;
   _n_ip       = (args.model == F_PHYS::SIMPLIFIED) ? 0 : args.n_ip;

   const auto          dAF_rho = compute_dAF(args.F_rho);
   const auto          dAF_phi = compute_dAF(args.F_phi);
   Material_Matrix_ptr dAF;
   dAF[RHO] = (!dAF_rho) ? nullptr : &dAF_rho;
   LOOP_ANGLES(_dim, k, { dAF[k] = (!dAF_phi) ? nullptr : &dAF_phi; })

   const double J_stop         = args.J_stop;
   const double eps            = args.eps;
   const int    iter_max       = args.iter_max;
   const int    iter_bisec_max = args.iter_bisec_max;
   const int    info           = args.info;
   const int    n_levels       = args.n_levels;
   const double tau_init       = args.tau;
   const double tau_incr       = args.tau_incr;
   const double lambda_min     = args.lambda_min;
   const double lambda_max     = args.lambda_max;
   const double p              = args.p;
   const double v_star         = args.v_star;
   const bool   precompute     = args.precompute_tensors;
   Samplesize   n_samples{1, 1, 1, 1};
   Bounds       bounds;
   Material     p_filt;

   // initialize data
   Material_Vector     x;
   std::vector<Matrix> dJ, Gamma;
   std::vector<Vector> epsilon;

   x[RHO] = args.rho0;
   x[PHI] = args.phi0;

   n_samples[RHO]  = args.n_rho;
   n_samples[PHI]  = args.n_phi;
   bounds.min[RHO] = args.rhoMin;
   bounds.max[RHO] = args.rhoMax;
   bounds.min[PHI] = args.phiMin;
   bounds.max[PHI] = args.phiMax;
   p_filt[RHO]     = args.p_filt_rho;
   p_filt[PHI]     = args.p_filt_phi;
   if (_dim == 3)
   {
      x[THETA] = args.theta0;
      x[PSI]   = args.psi0;

      n_samples[THETA]  = args.n_theta;
      n_samples[PSI]    = args.n_psi;
      bounds.min[THETA] = args.thetaMin;
      bounds.max[THETA] = args.thetaMax;
      bounds.min[PSI]   = args.psiMin;
      bounds.max[PSI]   = args.psiMax;
      p_filt[THETA]     = args.p_filt_phi;
      p_filt[PSI]       = args.p_filt_phi;
   }

   Material_Vector x_old = x;

   for (int e = 0; e < _n_el; ++e)
   {
      if (_phys_model == F_PHYS::SIMPLIFIED)
      {
         dJ.push_back(Matrix(n_VoigtNotation(_dim)));
      }
      if (_phys_model == F_PHYS::GENERALIZED)
      {
         Gamma.push_back(Matrix(n_VoigtNotation(_dim) * _n_ip));
         epsilon.push_back(Vector(n_VoigtNotation(_dim) * _n_ip));
      }
   }

   double J     = eval_J(x);
   double J_old = nan("1");

   // check initial data
   SGP_CHECK(lambda_min <= lambda_max, "lambda_min must be less than or equal to lamba_max!");
   SGP_CHECK(tau_init > 0, "tau_init must be greater than zero!");
   SGP_CHECK(tau_incr > 1, "tau_incr must be greater than one!");
   SGP_CHECK(n_levels > 0, "n_levels must be greater than zero!");
   LOOP_DESIGN_VAR(_dim, k, {
      SGP_CHECK(int(x[k].size()) == _n_el, DesignVariables::name[k] << "0 must have one entry per element!");
      SGP_CHECK(bounds.min[k] <= bounds.max[k],
                DesignVariables::name[k] << "_max must not be less than " << DesignVariables::name[k] << "_min!");
      SGP_CHECK(n_samples[k] > 2 || bounds.min[k] == bounds.max[k], "n_" << DesignVariables::name[k] << " must be at least 3!");
   });

   // initialize output
   Output output(info, args.outputfile);

   // prepare data for tensors
   LOOP_DESIGN_VAR(_dim, k, {
      if (bounds.max[k] == bounds.min[k])
      {
         // set n_samples to 1 s.th. x = bounds.min = bounds.max is considered exactly once
         n_samples[k] = 1;
      }
   })
   if (precompute)
   {
      timingTree[Timer::TOTAL].start();
      precompute_tensors(n_levels, bounds, n_samples, p);
      timingTree[Timer::TOTAL].stop();
   }

   while (1) // main loop
   {
      // check for external interrupt
      if (abort(_info))
      {
         return finalize(x, Status::ABORTED_BY_USER, output, summary);
      }
      // check 1st stopping criterion (convergence)
      if (_info.convergence_value <= J_stop)
      {
         return finalize(x, Status::CONVERGED, output, summary);
      }
      // check 2nd stopping criterion (max iterations)
      if (_info.n_iterations >= iter_max)
      {
         return finalize(x, Status::MAX_ITERATIONS, output, summary);
      }

      // increment iteration counter
      ++_info.n_iterations;

      // initialize timing for current iteration
      for (int t = 0; t != Timer::N_TIMER_SECTIONS; ++t)
      {
         timingTree[t].new_section();
      }
      timingTree[Timer::TOTAL].start();

      // initialize temporary variables
      int    n_subproblems          = 0;
      double lambda                 = 0;
      double tau                    = tau_init;
      double v                      = 0;
      bool   volume_bound_satisfied = false;

      // store/compute values corresponding to x_old = x_{iter - 1}
      J_old = J;
      x_old = x;
      timingTree[Timer::EVAL_DJ].start();
      if (_phys_model == F_PHYS::SIMPLIFIED)
      {
         eval_dJ(x_old, dJ);
      }
      if (_phys_model == F_PHYS::GENERALIZED)
      {
         eval_Gamma_epsilon(x_old, Gamma, epsilon);
      }
      timingTree[Timer::EVAL_DJ].stop();
      Material_Vector dAFR, dAFR_s, dAFR_c;
      if (_filt_model == F_REG::ANGLE)
      {
         dAFR = compute_dAFR(dAF, x_old, dAFR_t::PHI);
      }
      if (_filt_model == F_REG::SIN_COS)
      {
         dAFR_s = compute_dAFR(dAF, x_old, dAFR_t::SIN_2PHI);
         dAFR_c = compute_dAFR(dAF, x_old, dAFR_t::COS_2PHI);
      }

      do // globalization loop
      {
         double low    = lambda_min;
         double up     = lambda_max;
         double v_diff = 0;

         // volume bisection loop
         volume_bound_satisfied = false;
         for (int n = 0; n < iter_bisec_max && !volume_bound_satisfied; ++n)
         {
            lambda = 0.5 * (low + up);

            // solve subproblem
            timingTree[Timer::SOLVE_SUBPROBLEM_TOTAL].start();
            solve_subproblem(dJ,
                             Gamma,
                             epsilon,
                             x_old,
                             x,
                             dAF,
                             dAFR,
                             dAFR_s,
                             dAFR_c,
                             n_levels,
                             bounds,
                             n_samples,
                             p_filt,
                             p,
                             tau,
                             lambda,
                             precompute);
            timingTree[Timer::SOLVE_SUBPROBLEM_TOTAL].stop();
            ++n_subproblems;

            // check volume bound
            v      = dot(_v_FE, x[RHO]) / _v_tot;
            v_diff = v - v_star;

            // update lambda
            if (v_diff < 0)
            {
               up = lambda;
            }
            else
            {
               low = lambda;
            }

            // check stopping criterion
            volume_bound_satisfied = std::abs(v_diff) < eps;
         }

         // compute J(x)
         timingTree[Timer::EVAL_J].start();
         J = eval_J(x);
         timingTree[Timer::EVAL_J].stop();

         if (J > J_old) // we don't want to increment tau after final loop iteration
         {
            tau *= tau_incr;
         }

      } while (J > J_old);

      // evaluate F_merit for output
      auto F = eval_F_output(x, x_old, dAF, dAFR, dAFR_s, dAFR_c, p, p_filt, tau, lambda, dJ, Gamma, epsilon);

      timingTree[Timer::TOTAL].stop();

      _info.convergence_value = std::abs(J_old - J);

      if (!volume_bound_satisfied || (_info.n_iterations % info == 0))
      {
         output.iterinfo(_info.n_iterations,
                         J,
                         F[0],
                         F[1],
                         F[2],
                         F[3],
                         tau,
                         lambda,
                         v,
                         _info.convergence_value,
                         n_subproblems,
                         timingTree[Timer::TOTAL].time_section());

         if (!volume_bound_satisfied)
         {
            output.warning("Volume bound could not be satisfied!");
         }
      }
   }
}

std::array<double, 5> Algorithm::eval_F_output(const Material_Vector&     x,
                                               const Material_Vector&     x_old,
                                               const Material_Matrix_ptr& dAF,
                                               const Material_Vector&     dAFR,
                                               const Material_Vector&     dAFR_s,
                                               const Material_Vector&     dAFR_c,
                                               double                     p,
                                               const Material&            p_filt,
                                               double                     tau,
                                               double                     lambda,
                                               std::vector<Matrix>&       dJ,
                                               std::vector<Matrix>&       Gamma,
                                               std::vector<Vector>&       epsilon)
{
   std::array<double, 5> F{0, 0, 0, 0, 0};
   // omp reduction for array
#pragma omp declare reduction(+ : std::array<double, 5> : std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<double>()))

#pragma omp parallel for reduction(+ : F)
   for (int e = 0; e < _n_el; ++e)
   {
      Material xe, xe_old;
      Material dAF_ee, dAFR_e, dAFR_s_e, dAFR_c_e;
      Rotation sin, cos;

      LOOP_DESIGN_VAR(_dim, k, {
         xe[k]     = x[k][e];
         xe_old[k] = x_old[k][e];
         dAF_ee[k] = (!dAF[k]) ? 0.0 : dAF[k]->D[e];
         if (_filt_model == F_REG::ANGLE)
         {
            dAFR_e[k] = dAFR[k][e];
         }
         if (_filt_model == F_REG::SIN_COS)
         {
            dAFR_e[k]   = dAFR_s[k][e]; // we actually just require the RHO entry from here
            dAFR_s_e[k] = dAFR_s[k][e];
            dAFR_c_e[k] = dAFR_c[k][e];
         }
      })
      LOOP_ANGLES(_dim, k, {
         sin[k] = std::sin(xe[k]);
         cos[k] = std::cos(xe[k]);
      })

      double f_phys_e = 0;
      Matrix D_old(n_VoigtNotation(_dim));
      eval_D(xe_old, p, D_old, false);

      // compute simplified F_phys
      if (_phys_model == F_PHYS::SIMPLIFIED)
      {
         Matrix D_inv(n_VoigtNotation(_dim));
         eval_D(xe, p, D_inv, true);
         auto DdJD = D_old * dJ[e] * D_old;
         f_phys_e  = eval_F_phys(D_inv, DdJD);
      }
      // compute generalized F_phys
      if (_phys_model == F_PHYS::GENERALIZED)
      {
         Matrix D(n_VoigtNotation(_dim)), D_hat(n_VoigtNotation(_dim) * _n_ip), D_hat_old(n_VoigtNotation(_dim) * _n_ip);
         eval_D(xe, p, D, false);
         Matrix::extend_to_block_matrix(D, D_hat, _n_ip);
         Matrix::extend_to_block_matrix(D_old, D_hat_old, _n_ip);
         f_phys_e = eval_F_phys_gen(epsilon[e], Gamma[e], D_hat, D_hat_old);
      }

      F[0] += f_phys_e;
      F[0] += eval_F_vol(xe, _v_FE[e], lambda);
      F[0] += eval_F_reg(xe, dAF_ee, dAFR_e, dAFR_s_e, dAFR_c_e, p_filt);
      F[0] += eval_F_glob(xe, xe_old, tau);

      F[1] += f_phys_e;
      F[2] += eval_F_vol(xe, _v_FE[e], 1);
      F[3] += eval_F_reg(xe, dAF_ee, dAFR_e, dAFR_s_e, dAFR_c_e, Material{1, 1, 1, 1});
      F[4] += eval_F_glob(xe, xe_old, 1);
   }

   return F;
}

Material_Vector Algorithm::finalize(const Material_Vector& x, Status status, Output& output, bool verbose) const
{
   _info.status = status;

   Vector time(Timer::N_TIMER_SECTIONS);
   for (int t = 0; t < Timer::N_TIMER_SECTIONS; ++t)
   {
      time[t] = timingTree[t].time_total();
   }

   if (verbose)
   {
      output.statusinfo(_info);
      output.timinginfo(time);
   }

   return x;
}

Info Algorithm::getStatus()
{
   return _info;
}

/* solve separable problem for each element and write result to x
      @param dJ         gradient vector dJ(D_old)/dD_old
      @param x_old      material configuratio from previous iteration
      @param x          resulting new configuration
      @param dAF        (I-F)^T * (I-F)
      @param dAFR       (dAF - diag(dAF))x_old
      @param dAFR_s     (dAF - diag(dAF))sin(2*phi)
      @param dAFR_c     (dAF - diag(dAF))cos(2*phi)
      @param n_levels   number of refinement levels
      @param bounds     upper and lower bound for each design variable
      @param n_samples  number of samples for each design variable
      @param p_filt     filter penalty
      @param p          power law parameter
      @param tau        globalization penalty
      @param lambda     volume multiplier
      @param precompute tensors are precomputed
   */
void Algorithm::solve_subproblem(const std::vector<Matrix>& dJ,
                                 const std::vector<Matrix>& Gamma,
                                 const std::vector<Vector>& epsilon,
                                 const Material_Vector&     x_old,
                                 Material_Vector&           x,
                                 const Material_Matrix_ptr& dAF,
                                 const Material_Vector&     dAFR,
                                 const Material_Vector&     dAFR_s,
                                 const Material_Vector&     dAFR_c,
                                 const int                  n_levels,
                                 const Bounds&              bounds,
                                 const Samplesize&          n_samples,
                                 const Material&            p_filt,
                                 const double               p,
                                 const double               tau,
                                 const double               lambda,
                                 const bool                 precompute)
{
   Samplesize              n_samples_tot;        // number of total samples possible
   Material                delta;                // increments for finest mesh width
   std::vector<Samplesize> mesh_width(n_levels); // mesh width (w.r.t. indices) for each level

   for (const auto& k : DesignVariables::all3D)
   {
      n_samples_tot[k] = std::pow(n_samples[k] - 1, n_levels) + 1;

      if (n_samples_tot[k] > 1)
      {
         delta[k] = (bounds.max[k] - bounds.min[k]) / double(n_samples_tot[k] - 1);
      }

      mesh_width[0][k] = 1;
      int refinement   = std::max(1, n_samples[k] - 1);

      for (int lvl = 1; lvl < n_levels; ++lvl)
      {
         mesh_width[lvl][k] = refinement * mesh_width[lvl - 1][k];
      }
   }

// loop over elements
#pragma omp parallel for
   for (int e = 0; e < _n_el; ++e)
   {
      Material   cur;               // current trial material parameters
      Rotation   cos;               // cos(cur)
      Rotation   sin;               // sin(cur)
      Samplesize idx;               // index of current material parameters on finest grid
      Samplesize idx_begin;         // index of lower bound for current level
      Samplesize idx_end;           // index of upper bound for current level
      Samplesize idx_xe;            // index of chosen configuration which minimizes F
      Material   xe  = bounds.min;  // material parameters, s.th. xe = argmin_x F(x)
      double     F_e = INF;         // F(xe)
      Material   xe_old;            // material parameters from previous iteration;
      Material   dAFR_e;            // entry of dAFR corresponding to element e
      Material   dAFR_s_e;          // entry of dAFR_s corresponding to element e
      Material   dAFR_c_e;          // entry of dAFR_c corresponding to element e
      Material   dAF_ee;            // diagonal entry of dAF corresponding to element e
      double     v_FE_e = _v_FE[e]; // volume of element e
      Matrix     dJ_e;              // local component of dJ
      Matrix     Gamma_e;           // local component of Gamma
      Vector     epsilon_e;         // local component of epsilon
      if (_phys_model == F_PHYS::SIMPLIFIED)
      {
         dJ_e = dJ[e];
      }
      if (_phys_model == F_PHYS::GENERALIZED)
      {
         Gamma_e   = Gamma[e];   // local component of Gamma
         epsilon_e = epsilon[e]; // local component of epsilon
      }

      // loop over rho,phi,theta,psi
      LOOP_DESIGN_VAR(_dim, k, {
         xe_old[k] = x_old[k][e];
         dAF_ee[k] = (!dAF[k]) ? 0.0 : dAF[k]->D[e];
         if (_filt_model == F_REG::ANGLE)
         {
            dAFR_e[k] = dAFR[k][e];
         }
         if (_filt_model == F_REG::SIN_COS)
         {
            dAFR_e[k]   = dAFR_s[k][e]; // we actually just require the RHO entry from here
            dAFR_s_e[k] = dAFR_s[k][e];
            dAFR_c_e[k] = dAFR_c[k][e];
         }
      })

      for (const auto& k : DesignVariables::all3D)
      {
         idx_begin[k] = 0;
         idx_end[k]   = n_samples_tot[k] - 1;
         idx_xe[k]    = 0;
      }

      // data for simplified F_phys
      Matrix D_inv(n_VoigtNotation(_dim)), D_old(n_VoigtNotation(_dim));
      eval_D(xe_old, p, D_old);
      auto DdJD = D_old * dJ_e * D_old; // D(x^bar) dJ/dD D(x^bar)

      // data for generalized F_phys
      Matrix D(n_VoigtNotation(_dim)), D_hat(n_VoigtNotation(_dim) * _n_ip), D_hat_old(n_VoigtNotation(_dim) * _n_ip);
      if (_phys_model == F_PHYS::GENERALIZED)
      {
         Matrix::extend_to_block_matrix(D_old, D_hat_old, _n_ip);
      }

      // loop over refinement levels (lvl = 0 is the finest level)
      for (int lvl = int(n_levels) - 1; lvl >= 0; --lvl)
      {
         // loop over material parameters phi, theta, psi, rho
         for (idx[PHI] = idx_begin[PHI]; idx[PHI] <= idx_end[PHI]; idx[PHI] += mesh_width[lvl][PHI])
         {
            cur[PHI] = bounds.min[PHI] + idx[PHI] * delta[PHI];
            cos[PHI] = std::cos(cur[PHI]);
            sin[PHI] = std::sin(cur[PHI]);

            for (idx[THETA] = idx_begin[THETA]; idx[THETA] <= idx_end[THETA]; idx[THETA] += mesh_width[lvl][THETA])
            {
               cur[THETA] = bounds.min[THETA] + idx[THETA] * delta[THETA];
               cos[THETA] = std::cos(cur[THETA]);
               sin[THETA] = std::sin(cur[THETA]);

               for (idx[PSI] = idx_begin[PSI]; idx[PSI] <= idx_end[PSI]; idx[PSI] += mesh_width[lvl][PSI])
               {
                  cur[PSI] = bounds.min[PSI] + idx[PSI] * delta[PSI];
                  cos[PSI] = std::cos(cur[PSI]);
                  sin[PSI] = std::sin(cur[PSI]);

                  // prepare tensor data for xe
                  if (_phys_model == F_PHYS::SIMPLIFIED)
                  {
                     // D_inv <- (Q * D_core * Q^t)^-1
                     if (precompute)
                     {
                        D_inv = _D_inv_precomputed[idx[PHI]][idx[THETA]][idx[PSI]];
                     }
                     else
                     {
                        eval_D_rot(cos, sin, D_inv, true);
                     }
                  }
                  if (_phys_model == F_PHYS::GENERALIZED)
                  {
                     // D <- Q * D_core * Q^t
                     eval_D_rot(cos, sin, D, false);
                  }

                  for (idx[RHO] = idx_begin[RHO]; idx[RHO] <= idx_end[RHO]; idx[RHO] += mesh_width[lvl][RHO])
                  {
                     cur[RHO] = bounds.min[RHO] + idx[RHO] * delta[RHO];

                     // eval F_merit(cur)
                     double F = eval_F_vol(cur, v_FE_e, lambda);
                     F += eval_F_reg(cur, dAF_ee, dAFR_e, dAFR_s_e, dAFR_c_e, p_filt);
                     F += eval_F_glob(cur, xe_old, tau);
                     if (_phys_model == F_PHYS::SIMPLIFIED)
                     {
                        // rho^(-p)
                        double rho_mp = (precompute) ? _pow_rho_mp_precomputed[idx[RHO]] : std::pow(cur[RHO], -p);
                        // D_inv <- rho^(-p) * D_inv
                        auto D_inv_rho = D_inv.scaled(rho_mp);
                        F += eval_F_phys(D_inv_rho, DdJD);
                     }
                     if (_phys_model == F_PHYS::GENERALIZED)
                     {
                        // D <- rho^(p) * D
                        auto D_rho = D.scaled(std::pow(cur[RHO], p));
                        Matrix::extend_to_block_matrix(D_rho, D_hat, _n_ip);
                        F += eval_F_phys_gen(epsilon_e, Gamma_e, D_hat, D_hat_old);
                     }

                     // check whether cur is new minimum
                     if (F < F_e)
                     {
                        xe     = cur;
                        idx_xe = idx;
                        F_e    = F;
                     }
                  }
               }
            }
         }

         // define range for next finer level
         for (const auto& k : DesignVariables::all3D)
         {
            idx_begin[k] = std::max(idx_xe[k] - mesh_width[lvl][k], 0);
            idx_end[k]   = std::min(idx_xe[k] + mesh_width[lvl][k], n_samples_tot[k] - 1);
         }
      }

      // update local material design
      LOOP_DESIGN_VAR(_dim, k, x[k][e] = xe[k];)
   }
}

LDR_Matrix Algorithm::compute_dAF(const CSR_Matrix& F) const
{
   if (!F)
   {
      // F = id => dAF = 0
      return LDR_Matrix();
   }
   else
   {
      SGP_CHECK(F.rows() == _n_el && F.cols() == _n_el, "Dimension mismatch: F must be an n_el x n_el matrix!");

      auto ImF = CSR_Matrix::id(_n_el) - F; // I-F
      auto dAF = ImF.AtA();                 //(I-F)^T * (I-F)
      return LDR_Matrix(dAF);               // split dAF = L+D+R
   }
}

Material_Vector Algorithm::compute_dAFR(const Material_Matrix_ptr& dAF, const Material_Vector& x_bar, dAFR_t type)
{
   Material_Vector dAFR(x_bar);
   // dAFR = (dAF - diag(dAF)) x_bar
   LOOP_DESIGN_VAR(
       _dim,
       k,
       if (!dAF[k]) //
       {            // dAF = 0 => dAFR = 0
          std::memset(dAFR[k].data(), 0, sizeof(double));
       }    //
       else //
       {
          if (k == RHO || type == dAFR_t::PHI)
          {
             dAF[k]->LR.apply(x_bar[k], dAFR[k]);
          }
          else
          {
             Vector xk = x_bar[k];
             for (auto& phi : xk)
             {
                switch (type)
                {
                   case dAFR_t::SIN_2PHI:
                      phi = std::sin(2 * phi);
                      break;
                   case dAFR_t::COS_2PHI:
                      phi = std::cos(2 * phi);
                      break;
                   default:
                      break;
                }
             }
             dAF[k]->LR.apply(xk, dAFR[k]);
          }
       })
   return dAFR;
}

/* evaluate D = rho^p * Q * D_core * Q^t */
void Algorithm::eval_D(const Material& x, double p, Matrix& D, bool inverse) const
{
   Rotation cos, sin;
   LOOP_ANGLES(_dim, k, {
      cos[k] = std::cos(x[k]);
      sin[k] = std::sin(x[k]);
   })

   eval_D_rot(cos, sin, D, inverse);
   double sp = (inverse) ? -p : p;
   D.scale(std::pow(x[RHO], sp));
}

/* evaluate D_rot = Q * D_core * Q^t */
void Algorithm::eval_D_rot(const Rotation& cos, const Rotation& sin, Matrix& D_rot, bool inverse) const
{
   auto          Q = convert_to_Voigt(rotation_matrix(_dim, cos, sin, inverse));
   const Matrix& C = (inverse) ? _D_core_inv : _D_core;
   Matrix::syprd(Q, C, D_rot, inverse);
}

inline double Algorithm::eval_F_phys(const Matrix& D_inv, const Matrix& DdJD) const
{
   return -dot(DdJD, D_inv);
}

inline double
    Algorithm::eval_F_phys_gen(const Vector& epsilon, const Matrix& Gamma, const Matrix& D_hat, const Matrix& D_hat_old) const
{
   // ΔD = (hat{D}_old - hat{D})
   auto DeltaD = D_hat_old - D_hat;
   // M = (I + Γ⋅ΔD)
   auto M = Gamma * DeltaD;
   M.shift_diagonal(1);

   // v = M^{-1}⋅ε
   Vector v = M.apply_inverse(epsilon);

   return -dot(epsilon, DeltaD.apply(v));
}

inline double Algorithm::eval_F_vol(const Material& xe, double v_FE_e, double lambda) const
{
   // todo change either here or in pseudo code (or both?)
   double v_avg = _v_tot / _n_el;
   return lambda * xe[RHO] * v_FE_e / v_avg;
}

inline double Algorithm::eval_F_reg(const Material& xe,
                                    const Material& dAF_ee,
                                    const Material& dAFR_e,
                                    const Material& dAFR_s_e,
                                    const Material& dAFR_c_e,
                                    const Material& p_filt) const
{
   double F_reg = 0.0; // p_filt * sum_k(0.5xe_k^2 * dAF_ee + xe_k * dAFR_e_k)

   if (_filt_model == F_REG::ANGLE)
   {
      LOOP_DESIGN_VAR(_dim, k, { F_reg += p_filt[k] * (0.5 * xe[k] * dAF_ee[k] + dAFR_e[k]) * xe[k]; })
   }
   if (_filt_model == F_REG::SIN_COS)
   {
      F_reg += p_filt[RHO] * (0.5 * xe[RHO] * dAF_ee[RHO] + dAFR_e[RHO]) * xe[RHO];
      LOOP_ANGLES(_dim, k, {
         double s = std::sin(2 * xe[k]);
         double c = std::cos(2 * xe[k]);
         F_reg += p_filt[k] * (0.5 * s * dAF_ee[k] + dAFR_s_e[k]) * s;
         F_reg += p_filt[k] * (0.5 * c * dAF_ee[k] + dAFR_c_e[k]) * c;
      })
   }

   return F_reg;
}

inline double Algorithm::eval_F_glob(const Material& xe, const Material& xe_old, double tau) const
{
   double diff = 0.0; // ||xe - xe_old||^2

   LOOP_DESIGN_VAR(_dim, k, {
      double d = xe[k] - xe_old[k];
      diff += d * d;
   })

   return 0.5 * tau * diff;
}

void Algorithm::precompute_tensors(const int n_levels, const Bounds& bounds, const Samplesize& n_samples, double p)
{
   // init delta
   Material   delta{1, 1, 1, 1};
   Samplesize n_samples_tot;
   LOOP_DESIGN_VAR(_dim, k, {
      double intervall = bounds.max[k] - bounds.min[k];

      // total number of samples = (n_s - 1)^n_ell + 1
      n_samples_tot[k] = std::pow(n_samples[k] - 1, n_levels) + 1;

      if (intervall > 0)
      {
         delta[k] = intervall / double(n_samples_tot[k] - 1);
      }
   })

   Material   x;
   Rotation   cos; // cos(x)
   Rotation   sin; // sin(x)
   Samplesize idx; // index of current material parameter

   // loop over material parameters phi, theta, psi
   _D_inv_precomputed = std::vector<std::vector<std::vector<Matrix>>>(n_samples_tot[PHI]);

   for (idx[PHI] = 0; idx[PHI] < n_samples_tot[PHI]; ++idx[PHI])
   {
      x[PHI]   = bounds.min[PHI] + idx[PHI] * delta[PHI];
      cos[PHI] = std::cos(x[PHI]);
      sin[PHI] = std::sin(x[PHI]);

      _D_inv_precomputed[idx[PHI]] = std::vector<std::vector<Matrix>>(n_samples_tot[THETA]);

      for (idx[THETA] = 0; idx[THETA] < n_samples_tot[THETA]; ++idx[THETA])
      {
         x[THETA]   = bounds.min[THETA] + idx[THETA] * delta[THETA];
         cos[THETA] = std::cos(x[THETA]);
         sin[THETA] = std::sin(x[THETA]);

         _D_inv_precomputed[idx[PHI]][idx[THETA]] = std::vector<Matrix>(n_samples_tot[PSI]);

         for (idx[PSI] = 0; idx[PSI] < n_samples_tot[PSI]; ++idx[PSI])
         {
            x[PSI]   = bounds.min[PSI] + idx[PSI] * delta[PSI];
            cos[PSI] = std::cos(x[PSI]);
            sin[PSI] = std::sin(x[PSI]);

            _D_inv_precomputed[idx[PHI]][idx[THETA]][idx[PSI]] = Matrix(n_VoigtNotation(_dim));
            eval_D_rot(cos, sin, _D_inv_precomputed[idx[PHI]][idx[THETA]][idx[PSI]], true);
         }
      }
   }

   // loop over density rho
   _pow_rho_mp_precomputed = std::vector<double>(n_samples_tot[RHO]);

   for (idx[RHO] = 0; idx[RHO] < n_samples_tot[RHO]; ++idx[RHO])
   {
      x[RHO] = bounds.min[RHO] + idx[RHO] * delta[RHO];

      _pow_rho_mp_precomputed[idx[RHO]] = std::pow(x[RHO], -p);
   }
}

} // namespace sgp
