from sympy import *
from sympy.matrices import Matrix, zeros, ones, diag, eye
from sys import argv

def get_R_2d(α):
    R = zeros(2,2)

    R[0, 0] = cos(α)
    R[0, 1] = -sin(α)
    R[1, 0] = sin(α)
    R[1, 1] = cos(α)

    return R


def get_R_3d(α, β, γ):
    # 3d rotation of stiffness tensor(5 x 5 entries) by three angles
    # order: rotate first about z-axis, then y-axis, then x-axis
    # α: angle for rotation about z-axis
    # β: angle for rotation about y-axis
    # γ: angle for rotation about x-axis

    R = zeros(3,3)

    R[0, 0] = cos(β) * cos(γ)
    R[0, 1] = -cos(β) * sin(γ)
    R[0, 2] = sin(β)
    R[1, 0] = cos(α) * sin(γ) + sin(α) * sin(β) * cos(γ)
    R[1, 1] = cos(α) * cos(γ) - sin(α) * sin(β) * sin(γ)
    R[1, 2] = -sin(α) * cos(β)
    R[2, 0] = sin(α) * sin(γ) - cos(α) * sin(β) * cos(γ)
    R[2, 1] = sin(α) * cos(γ) + cos(α) * sin(β) * sin(γ)
    R[2, 2] = cos(α) * cos(β)

    return R


def convert_R_to_Voigt(R):
    dim = R.rows
    Q = None

    if dim == 2:
        Q = zeros(3,3)

        Q[0,0] = R[0,0]*R[0,0]
        Q[0,1] = R[0,1]*R[0,1]
        Q[0,2] = 2.0*R[0,0]*R[0,1]
        Q[1,0] = R[1,0]*R[1,0]
        Q[1,1] = R[1,1]*R[1,1]
        Q[1,2] = 2.0*R[1,0]*R[1,1]
        Q[2,0] = R[0,0]*R[1,0]
        Q[2,1] = R[0,1]*R[1,1]
        Q[2,2] = R[0,0]*R[1,1] + R[0,1]*R[1,0]

    if dim == 3:
        Q = zeros(6, 6)

        Q[0, 0] = R[0, 0] * R[0, 0]
        Q[0, 1] = R[0, 1] * R[0, 1]
        Q[0, 2] = R[0, 2] * R[0, 2]
        Q[0, 3] = 2.0 * R[0, 1] * R[0, 2]
        Q[0, 4] = 2.0 * R[0, 0] * R[0, 2]
        Q[0, 5] = 2.0 * R[0, 0] * R[0, 1]

        Q[1, 0] = R[1, 0] * R[1, 0]
        Q[1, 1] = R[1, 1] * R[1, 1]
        Q[1, 2] = R[1, 2] * R[1, 2]
        Q[1, 3] = 2.0 * R[1, 1] * R[1, 2]
        Q[1, 4] = 2.0 * R[1, 0] * R[1, 2]
        Q[1, 5] = 2.0 * R[1, 0] * R[1, 1]

        Q[2, 0] = R[2, 0] * R[2, 0]
        Q[2, 1] = R[2, 1] * R[2, 1]
        Q[2, 2] = R[2, 2] * R[2, 2]
        Q[2, 3] = 2.0 * R[2, 1] * R[2, 2]
        Q[2, 4] = 2.0 * R[2, 0] * R[2, 2]
        Q[2, 5] = 2.0 * R[2, 0] * R[2, 1]

        Q[3, 0] = R[1, 0] * R[2, 0]
        Q[3, 1] = R[1, 1] * R[2, 1]
        Q[3, 2] = R[1, 2] * R[2, 2]
        Q[3, 3] = R[1, 1] * R[2, 2] + R[1, 2] * R[2, 1]
        Q[3, 4] = R[1, 0] * R[2, 2] + R[1, 2] * R[2, 0]
        Q[3, 5] = R[1, 0] * R[2, 1] + R[1, 1] * R[2, 0]

        Q[4, 0] = R[0, 0] * R[2, 0]
        Q[4, 1] = R[0, 1] * R[2, 1]
        Q[4, 2] = R[0, 2] * R[2, 2]
        Q[4, 3] = R[0, 1] * R[2, 2] + R[0, 2] * R[2, 1]
        Q[4, 4] = R[0, 0] * R[2, 2] + R[0, 2] * R[2, 0]
        Q[4, 5] = R[0, 0] * R[2, 1] + R[0, 1] * R[2, 0]

        Q[5, 0] = R[0, 0] * R[1, 0]
        Q[5, 1] = R[0, 1] * R[1, 1]
        Q[5, 2] = R[0, 2] * R[1, 2]
        Q[5, 3] = R[0, 1] * R[1, 2] + R[0, 2] * R[1, 1]
        Q[5, 4] = R[0, 0] * R[1, 2] + R[0, 2] * R[1, 0]
        Q[5, 5] = R[0, 0] * R[1, 1] + R[0, 1] * R[1, 0]

    return Q


def printMatrix(M):
    for i in range(M.rows):
        print(list(M[i,:]))


if __name__ == '__main__':

    dim = int(argv[1])

    φ, θ, ψ = symbols('φ θ ψ')
    sφ, sθ, sψ = symbols('sφ sθ sψ')
    cφ, cθ, cψ = symbols('cφ cθ cψ')

    C = zeros(6, 6)
    Ci = zeros(6, 6)
    # inverse has same structure as C -> we proceed formally: γ_ij := [C^{-1}]_ij

    for i in range(6):
        for j in range(i,max(i+1,3)):
            C[i, j] = C[j, i] = symbols('c%d%d'%(i+1, j+1))
            Ci[i,j] = Ci[j,i] = symbols('γ%d%d'%(i+1, j+1))

    # printMatrix(C)
    print('C^-1 = ')
    printMatrix(Ci)

    # Ci = simplify(C.inv())
    # printMatrix(Ci)
    if (dim == 2):
        R = get_R_2d(φ)
    else:
        R = get_R_3d(φ, θ, ψ)

    Q = convert_R_to_Voigt(R)
    Qi = convert_R_to_Voigt(R.transpose())



    Q_tilde = Q.subs([(sin(φ),sφ), (cos(φ),cφ), (sin(θ),sθ), (cos(θ),cθ), (sin(ψ),sψ), (cos(ψ),cψ)])

    print('Q = ')
    # printMatrix(Q)
    printMatrix(Q_tilde)

    print('R*R^T = ')
    printMatrix(simplify(R*R.transpose()))
    print('Q(R)*Q(R^T) = ')
    # QQi = Q*Qi
    QQi = simplify(Q*Qi)
    print(QQi)

    n = 10
    d = 2*pi/n
    I = eye(Q.rows)

    for i in range(n):
        for j in range(n):
            for k in range(n):
                print("φ = %f, θ = %f, ψ = %f" %(i*d, j*d, k*d))
                tst = QQi.subs([(φ,i*d), (θ,j*d), (ψ,k*d)]).evalf()
                if any(el > 1e-20 for el in list(abs(tst - I))):
                    print(QQi.subs([(φ,i*d), (θ,j*d), (ψ,k*d)]).evalf())



    # Qt = Q.transpose()

    # Qi = Q.inv()
    # Qi_tilde = Qi.subs([(sin(φ), sφ), (cos(φ), cφ), (sin(θ), sθ), (cos(θ), cθ), (sin(ψ), sψ), (cos(ψ), cψ)])
    # print('Q^-1 = ')
    # printMatrix(Qi)
    # printMatrix(Qi_tilde)



    # [1.0*cos(ψ)**2, 1.0*sin(φ)**2*sin(ψ)**2, 1.0*sin(ψ)**2*cos(φ)**2, -2.0*sin(φ)*sin(ψ)**2*cos(φ), 0.5*sin(φ - 2*ψ) - 0.5*sin(φ + 2*ψ), 0.5*cos(φ - 2*ψ) - 0.5*cos(φ + 2*ψ)]
    # [0, 1.0*cos(φ)**2, 1.0*sin(φ)**2, 1.0*sin(2*φ), 0, 0]
    # [1.0*sin(ψ)**2, 1.0*sin(φ)**2*cos(ψ)**2, 1.0*cos(φ)**2*cos(ψ)**2, -2.0*sin(φ)*cos(φ)*cos(ψ)**2, -0.5*sin(φ - 2*ψ) + 0.5*sin(φ + 2*ψ), -0.5*cos(φ - 2*ψ) + 0.5*cos(φ + 2*ψ)]
    # [0, -0.25*sin(2*φ - ψ) - 0.25*sin(2*φ + ψ), 0.25*sin(2*φ - ψ) + 0.25*sin(2*φ + ψ), 1.0*cos(2*φ)*cos(ψ), 1.0*sin(φ)*sin(ψ), 1.0*sin(ψ)*cos(φ)]
    # [0.5*sin(2*ψ), -1.0*sin(φ)**2*sin(ψ)*cos(ψ), -1.0*sin(ψ)*cos(φ)**2*cos(ψ), 0.25*cos(2*(φ - ψ)) - 0.25*cos(2*(φ + ψ)), 1.0*cos(φ)*cos(2*ψ), -1.0*sin(φ)*cos(2*ψ)]
    # [0, 0.25*cos(2*φ - ψ) - 0.25*cos(2*φ + ψ), -0.25*cos(2*φ - ψ) + 0.25*cos(2*φ + ψ), -1.0*sin(ψ)*cos(2*φ), 1.0*sin(φ)*cos(ψ), 1.0*cos(φ)*cos(ψ)]

    # Qti = Qi.transpose()

    # printMatrix(Q)
    # print("\n")
    # printMatrix(Qt)
    # print("\n")
    # print(simplify(Qi[0,0]))
    # printMatrix(Qi)

    # QCQt = simplify(Q*C*Qt)
    # print('QCQ^t = ')
    # printMatrix(QCQt)

    #! this is what we actually want:
    # QCQti = simplify(Qti * Ci * Qi)
    # (with given Ci)



