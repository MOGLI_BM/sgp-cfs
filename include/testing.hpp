/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <iostream>
#include <string>

namespace sgp {
namespace test {

template <class ITERABLE>
inline void check_almost_zero(const ITERABLE& x, const std::string& err_msg, const double eps = 1e-14)
{
   bool error = false;
   for (const auto& el : x)
   {
      if (std::abs(el) > eps)
      {
         std::cerr << "ERROR: " << err_msg << std::endl;
         error = true;
         break;
      }
   }

   if (error)
   {
      std::cerr << "    diff = ";
      for (const auto& el : x)
      {
         std::cerr << el << " ";
      }
      std::cerr << std::endl;

      exit(1);
   }
}

template <>
inline void check_almost_zero<double>(const double& x, const std::string& err_msg, const double eps)
{
   std::vector<double> xx{x};
   return check_almost_zero(xx, err_msg, eps);
}

} // namespace test
} // namespace sgp
