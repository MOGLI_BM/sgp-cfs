/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include <cmath>
#include <map>

#include "random.hpp"
#include "rotation.hpp"
#include "testing.hpp"

using namespace sgp;
using namespace test;

constexpr double PI         = 3.14159265358979323;
const double     SQRT_2     = std::sqrt(2.0);
const double     INV_SQRT_2 = 1.0 / std::sqrt(2.0);

std::map<int, std::vector<Vector>> e; // unit vectors

void init_e()
{
   std::vector<Vector> e2, e3;

   for (int i = 0; i < 3; ++i)
   {
      e3.push_back(Vector(3));
      e3[i][i] = 1;
      if (i < 2)
      {
         e2.push_back(Vector(2));
         e2[i][i] = 1;
      }
   }

   e[2] = e2;
   e[3] = e3;
}

Matrix compute_rotation_matrix(int dim, const Rotation& rot, bool inverse = false)
{
   // compute sin and cosin
   Rotation c, s;
   if (dim == 2)
   {
      c[0] = std::cos(rot[2]);
      s[0] = std::sin(rot[2]);
   }
   else
   {
      for (int i = 0; i < 3; ++i)
      {
         c[i] = std::cos(rot[i]);
         s[i] = std::sin(rot[i]);
      }
   }

   // compute rotation matrix
   return rotation_matrix(dim, c, s, inverse);
}

void rotation_apply_test(int                          dim,
                         const Rotation&                 rot,
                         const std::vector<Vector>&      target,
                         const std::vector<std::string>& target_name)
{
   std::cout << "TEST: sgp::rotation_matrix(" << dim << ", cos,sin)\n";

   auto R = compute_rotation_matrix(dim, rot);

   // apply rotation to unit vectors
   for (int i = 0; i < dim; ++i)
   {
      auto        result = R.apply(e[dim][i]);
      auto        err    = diff(result, target[i]);
      std::string msg    = "R*e" + std::to_string(i) + " != " + target_name[i];
      check_almost_zero(err, msg);
   }
}

void inverse_rotation_tensor_test(int dim, const Rotation& rot)
{
   std::cout << "TEST: sgp::convert_to_Voigt(R)\n";

   auto R  = compute_rotation_matrix(dim, rot);
   auto Rt = compute_rotation_matrix(dim, rot, true);
   auto Q  = convert_to_Voigt(R);  // Q(R)
   auto Qi = convert_to_Voigt(Rt); // Q(R^t)

   // invert Q
   Q.inv();

   auto        err = diff(Q, Qi);
   std::string msg = "Q(R^t) != inv(Q(R))";
   check_almost_zero(err, msg);
}

void zero_rotation_test(int dim)
{
   std::cout << "TEST: rot(0,0,0) = id for " << dim << "D tensors.\n";

   auto   R = compute_rotation_matrix(dim, Rotation{0, 0, 0}); //R(0,0,0)
   auto   Q = convert_to_Voigt(R);                             // Q(R(0,0,0))
   Matrix id(n_VoigtNotation(dim));                                             // id
   for (int i = 0; i < id.rows(); ++i)
   {
      id(i, i) = 1;
   }

   auto        err = diff(Q, id);
   std::string msg = "Q(R(0,0,0)) != id";
   check_almost_zero(err, msg);
}

int main(int argc, char const* argv[])
{
   // initialize unit vectors
   init_e();

   // initialize test cases
   std::map<double, Rotation> rot{
       {45, {0, 0, PI / 4.0}}, {90, {0, 0, PI / 2.0}}, {-45, {0, 0, -PI / 4.0}}, {-90, {0, 0, -PI / 2.0}}};

   std::map<double, std::vector<Vector>> rot_e // expected result R*e_(1,2,3)
       {{90, {{0, 1, 0}, {-1, 0, 0}, {0, 0, 1}}},
        {-90, {{0, -1, 0}, {1, 0, 0}, {0, 0, 1}}},
        {45, {{INV_SQRT_2, INV_SQRT_2, 0}, {-INV_SQRT_2, INV_SQRT_2, 0}, {0, 0, 1}}},
        {-45, {{INV_SQRT_2, -INV_SQRT_2, 0}, {INV_SQRT_2, INV_SQRT_2, 0}, {0, 0, 1}}}};

   std::map<double, std::vector<std::string>> name_rot_e // name of expected result
       {{90, {"e2", "-e1", "e3"}},
        {-90, {"-e2", "e1", "e3"}},
        {45, {"1/sqrt(2) [1,1,0]", "1/sqrt(2) [-1,1,0]", "e3"}},
        {-45, {"1/sqrt(2) [1,-1,0]", "1/sqrt(2) [1,1,0]", "e3"}}};

   // random engine
   const int n_cases = 50;
   Random       random_angle(-PI, PI, 17);

   // run tests
   for (int dim = 2; dim <= 3; ++dim)
   {
      // Q(0)
      zero_rotation_test(dim);

      for (const auto& el : rot)
      {
         auto phi = el.first;
         auto rot = el.second;

         std::cout << "\ntesting with rotation " << rot << ":\n";

         // apply R
         rotation_apply_test(dim, rot, rot_e[phi], name_rot_e[phi]);
      }

      for (int i = 0; i < n_cases; ++i)
      {
         Rotation rot{random_angle.real(), random_angle.real(), random_angle.real()};
         std::cout << "\ntesting with rotation " << rot << ":\n";

         // inv(Q)
         inverse_rotation_tensor_test(dim, rot);
      }

      std::cout << "\n";
   }

   return 0;
}
