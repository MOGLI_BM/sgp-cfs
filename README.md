# C++ library for optimization of material parameters using sequencial global programming (SGP)

## Build instructions

### Requirements
* Intel MKL
* CMake
* OpenMP

Clone the repository
```
$ git clone https://gitlab.com/MOGLI_BM/sgp-cfs.git
```
Build the example
```
$ cd sgp_cfs
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Usage

Run the example
```
$ ./src/example
```
To include sgp into you'r own software, you need to implement a class
derived from `sgp::Algorithm` declared in [sgp.hpp](https://gitlab.com/MOGLI_BM/sgp-cfs/-/blob/master/include/sgp.hpp).
This derived class must implement the member functions `sgp::Algorithm::eval_J` and `sgp::Algorithm::eval_dJ` as shown in [example.cpp](https://gitlab.com/MOGLI_BM/sgp-cfs/-/blob/master/src/example.cpp).


## Development

When contributing to the project, make sure to keep formatting intact and always use `clang-format'!

Even more important: Always make sure that your contribution does not interfere with existing features by running all of the tests:
```
$ cd MY_BUILD_DIR
$ cd tests
$ ctest
```
