/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <iomanip>

namespace sgp {

class Format
{
 public:
   enum Type
   {
      DEFAULT,
      FIXED,
      SCIENTIFIC,
      LEFT,
      RIGHT
   };

   Format(int width, int precision, Type type = DEFAULT)
   : W(width)
   , P(precision)
   , ftype(type)
   {}

   friend std::ostream& operator<<(std::ostream& out, const Format& f)
   {
      out << std::setw(f.W) << std::setprecision(f.P);
      switch (f.ftype)
      {
         case Format::DEFAULT:
            out << std::defaultfloat;
            // out.setf(std::defaultfloat, std::ios_base::floatfield);
            break;
         case Format::FIXED:
            out << std::fixed;
            // out.setf(std::fixed, std::ios_base::floatfield);
            break;
         case Format::SCIENTIFIC:
            out << std::scientific;
            // out.setf(std::scientific, std::ios_base::floatfield);
            break;
         case Format::LEFT:
            out << std::left;
            // out.setf(std::ios_base::left, std::ios_base::adjustfield);
            break;
         case Format::RIGHT:
            out << std::right;
            // out.setf(std::ios_base::right, std::ios_base::adjustfield);
            break;
      }

      // out.precision(f.P);
      // out.width(f.W);
      return out;
   }

 private:
   const int  W;
   const int  P;
   const Type ftype;
};

} // namespace sgp