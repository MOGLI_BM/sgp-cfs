/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <iostream>
#include <vector>

namespace sgp {

using Vector = std::vector<double>;

/* computes the euclidean dot product <x,y> = sum_i x_i*y_i*/
double dot(const Vector& x, const Vector& y);
/* computes y = a*x */
Vector scale(double a, const Vector& x);
/* computes z = x+y */
Vector sum(const Vector& x, const Vector& y);
/* computes z = x-y */
Vector diff(const Vector& x, const Vector& y);

template <typename T>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T>& v)
{
   for (const auto& el : v)
      out << el << " ";

   return out;
}

} // namespace sgp
