/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include "material_parameters.hpp"
#include "tensor.hpp"

namespace sgp {

/* create rotation matrix R(φ[,θ,ψ])
   @param dim     spacial dimension (2 or 3)
   @param cos     cosine of rotation angle(s)
   @param sin     sine of rotation angle(s)
   @param inverse compute R^-1 = R^t instead
   @returns       Rotation Matrix R (or R^-1)
*/
Matrix rotation_matrix(int dim, const Rotation& cos, const Rotation& sin, bool inverse = false);

/* convert matrix to Voigt notation
   @param R  dxd matrix
   @returns  R in Voigt notation
*/
Matrix convert_to_Voigt(const Matrix& R);

} // namespace sgp
