sgp_add_test(
    NAME CSR_test
    LIBS sgp
)

sgp_add_test(
    NAME rotation_test
    LIBS sgp
)

sgp_add_test(
    NAME sgp_helper_test
    LIBS sgp
)

sgp_add_test(
    NAME tensor_test
    LIBS sgp
)