/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <stdexcept>

#include "datatypes.hpp"
#include "vector.hpp"

namespace sgp {

// compute matrix dimensions of 2D/3D tensor in Voigt notation
inline int n_VoigtNotation(int dim)
{
   switch (dim)
   {
      case 2:
         return 3;
      case 3:
         return 6;
      default:
         return (dim + 1) * dim / 2;
   }
}

// wrapper class for NxN matrices
class Matrix : public Vector
{
 public:
   /* Ctor
        @params n nuber of rows/cols
    */
   Matrix(int n = 0)
   : Vector(n*n)
   , N(n)
   {}

   /* Ctor
        @params n nuber of rows/cols
        @params dat     pointer to row major matrix data
    */
   Matrix(int n, const double* dat)
   : Matrix(n)
   {
      setData(dat);
   }

   /* copy and inverse Ctor
      @param B       Matrix that shall be copied
      @param inverse copy the inverse of B instead
   */
   Matrix(const Matrix& B, bool inverse)
   : Matrix(B)
   {
      if (inverse)
         this->inv();
   }

   // compute C = A*B
   Matrix operator*(const Matrix& B) const;
   // compute C = A+B
   Matrix operator+(const Matrix& B) const;
   // compute C = A-B
   Matrix operator-(const Matrix& B) const;

   /* compute symmetric triple product Q*C*Q^t
      @param Q          non-symmetric Matrix
      @param C          symmetric Matrix
      @param D          pre-allocated memory for result
      @param reversed   if true, compute Q^t*C*Q instead
      @returns either Q*C*Q^t or Q^t*C*Q
   */
   static Matrix& syprd(const Matrix& Q, const Matrix& C, Matrix& D, bool reversed = false);

   // multiply all components of M by s, i.e. M <- s*M
   inline void scale(double s)
   {
      for (auto& t : *this)
      {
         t *= s;
      }
   }

   // return s*this
   inline Matrix scaled(double s)
   {
      Matrix sM = *this;
      sM.scale(s);
      return sM;
   }

   Vector apply(const Vector& x, bool transpose = false) const;

   /* solve the linear system Ax = b
      @param b right hand side
      @return solution x
   */
   Vector apply_inverse(const Vector& b) const;

   // invert the matrix
   void inv();

   // transpose the matrix
   void trans();

   // shift diagonal of matrix A_ii += scalar
   void shift_diagonal(double scalar);

   // get matrix entry from i-th row and j-th column
   inline const double& operator()(int i, int j) const { return (*this)[N * i + j]; }

   // get matrix entry from i-th row and j-th column
   inline double& operator()(int i, int j) { return (*this)[N * i + j]; }

   inline int rows() const { return N; }
   inline int cols() const { return N; }

   friend std::ostream& operator<<(std::ostream& out, const Matrix& M);

   /* extend matrix m⨯m matrix A to nm⨯nm by setting
      A_ex = [A 0 ... 0; 0 A 0 ... 0; ... ; 0 ... 0 A]
      @param A    input matrix m⨯m
      @param A_ex output matrix nm⨯nm
      @param n    number of times, A will be put into A_ex
   */
   static void extend_to_block_matrix(const Matrix& A, Matrix& A_ex, int n);

 protected:
   void setData(const double* dat);

   int N;
};

} // namespace sgp
