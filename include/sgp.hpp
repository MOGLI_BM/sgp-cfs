/*
   Base class for sequential global programming (SGP) algorithm.

   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include "csr_matrix.hpp"
#include "datatypes.hpp"
#include "material_parameters.hpp"
#include "output.hpp"
#include "params.hpp"
#include "status.hpp"
#include "tensor.hpp"
#include "timer.hpp"
#include "vector.hpp"

namespace sgp {

// forward declaration required
namespace test {
class SGP_Testclass;
} // namespace test

class Algorithm
{
 public:
   /* Ctor
      @param args    parameters to initialize the algorithm
   */
   Algorithm(const InitParams& args);

   /* solve optimization problem
      @param args    optimization parameters, i.e., number of iterations etc.
      @param summary add summary to output-file
      @return        material design x s.th. x = min_y J(y) for given energy
                     functional J
   */
   Material_Vector solve(const SolveParams& args, bool summary = true);

   /* solve optimization problem in a two step scheme:
      The initialisation step comprises sgp iterations until convergence,
         whithout volume bisection. Meaning, that the volume bound is ignored
         and the subproblems will be solved only once with lambda = lambda_init.
      The second step is equivalent to calling solve(args) with using the
         solution from the init-step as initial guess.
      @param args          optimization parameters, i.e., number of iterations etc.
      @param lambda_init   volume multiplier for the initialization step
      @return        material design x s.th. x = min_y J(y) for given energy
                     functional J
   */
   Material_Vector solve(const SolveParams& args, double lambda_init);

   /* get status information, i.e. convergence, number of iterations, etc
   */
   Info getStatus();

 protected:
   //!! callback functions eval_Gamma_epsilon, eval_dJ and eval_J -> to be implemented in derived class !!

   /* compute Gamma and epsilon for current design x
      @param x       current material design s.th. x[RHO][e] is the density rho
                     of element e and analogously for phi,theta and psi.
      @param Gamma   preallocated output vector for Γ, s.th. Gamma[e] corresponds to element e.
      @param epsilon preallocated output vector for ε, s.th. epsilon[e] corresponds to element e.
   */
   virtual void eval_Gamma_epsilon(const Material_Vector& x, std::vector<Matrix>& Gamma, std::vector<Vector>& epsilon) const = 0;

   /* compute the gradient of the energy functional w.r.t. design variable x
      @param x    current material design s.th. x[RHO][e] is the density rho
                  of element e and analogously for phi,theta and psi.
      @param dJ   preallocated output vector for the gradient s.th. dJ[e] is the part of dJ(D(x))/dD(x)
                  corresponding to D(x[e]).
   */
   virtual void eval_dJ(const Material_Vector& x, std::vector<Matrix>& dJ) const = 0;

   /* compute the value of the energy functional J for the current design
      @param x    current material design s.th. x[RHO][e] is the density rho
                  of element e and analogously for phi,theta and psi.
      @return     J(x)
   */
   virtual double eval_J(const Material_Vector& x) const = 0;

   /* triggers aborting the execution of the algorithm
      May be implemented in derived class if needed.
      @param info  current status information of optimization process
      @return whether or not the execution shall be aborted
   */
   virtual bool abort(const Info& info) const { return false; }

   ////////////////////////////////////////////////////////////////////////

   const int _dim;  // spacial dimensions of simulation domain (2 or 3)
   const int _n_el; // number of finite elements

   F_PHYS _phys_model; // model for F_phys
   int    _n_ip;       // number of integration points per element

 private:
   //======== internal data and helper functions ==========

   /* compute F_merit(x)
      @param x          resulting new configuration
      @param x_old      material configuratio from previous iteration
      @param dAF        (I-F)^T * (I-F)
      @param dAFR       (dAF - diag(dAF))x_old
      @param dAFR_s     (dAF - diag(dAF))sin(2*phi)
      @param dAFR_c     (dAF - diag(dAF))cos(2*phi)
      @param p          power law parameter
      @param dJ         gradient vector dJ(D_old)/dD_old
      @param Gamma      vector of Γ_e for all elements
      @param epsilon    vector of ε_e for all elements
      @return [F_merit, F_phys, F_vol, F_reg, F_glob]
   */
   std::array<double, 5> eval_F_output(const Material_Vector&     x,
                                       const Material_Vector&     x_old,
                                       const Material_Matrix_ptr& dAF,
                                       const Material_Vector&     dAFR,
                                       const Material_Vector&     dAFR_s,
                                       const Material_Vector&     dAFR_c,
                                       double                     p,
                                       const Material&            p_filt,
                                       double                     tau,
                                       double                     lambda,
                                       std::vector<Matrix>&       dJ,
                                       std::vector<Matrix>&       Gamma,
                                       std::vector<Vector>&       epsilon);

   Material_Vector finalize(const Material_Vector& x, Status status, Output& output, bool verbose) const;

   /* solve separable problem for each element and write result to x
      @param dJ         gradient vector dJ(D_old)/dD_old
      @param Gamma      vector of Γ_e for all elements
      @param epsilon    vector of ε_e for all elements
      @param x_old      material configuratio from previous iteration
      @param x          resulting new configuration
      @param dAF        (I-F)^T * (I-F)
      @param dAFR       (dAF - diag(dAF))x_old
      @param dAFR_s     (dAF - diag(dAF))sin(2*phi)
      @param dAFR_c     (dAF - diag(dAF))cos(2*phi)
      @param n_levels   number of refinement levels
      @param bounds     upper and lower bound for each design variable
      @param n_samples  number of samples for each design variable
      @param p_filt     filter penalty
      @param p          power law parameter
      @param tau        globalization penalty
      @param lambda     volume multiplier
      @param precompute tensors are precomputed
   */
   void solve_subproblem(const std::vector<Matrix>& dJ,
                         const std::vector<Matrix>& Gamma,
                         const std::vector<Vector>& epsilon,
                         const Material_Vector&     x_old,
                         Material_Vector&           x,
                         const Material_Matrix_ptr& dAF,
                         const Material_Vector&     dAFR,
                         const Material_Vector&     dAFR_s,
                         const Material_Vector&     dAFR_c,
                         const int                  n_levels,
                         const Bounds&              bounds,
                         const Samplesize&          n_samples,
                         const Material&            p_filt,
                         const double               p,
                         const double               tau,
                         const double               lambda,
                         const bool                 precompute);

   LDR_Matrix compute_dAF(const CSR_Matrix& F) const;

   enum class dAFR_t
   {
      PHI,
      SIN_2PHI,
      COS_2PHI
   };

   Material_Vector compute_dAFR(const Material_Matrix_ptr& dAF, const Material_Vector& x_bar, dAFR_t type = dAFR_t::PHI);

   /* evaluate D = rho^p * Q * D_core * Q^t */
   void eval_D(const Material& x, double p, Matrix& D, bool inverse = false) const;

   /* evaluate D_rot = Q * D_core * Q^t */
   void eval_D_rot(const Rotation& cos, const Rotation& sin, Matrix& D_rot, bool inverse = false) const;

   /* evaluate F_pys (SIMPLIFIED)
      @param D_inv      inverse stiffness tensor D(xe)^(-1)
      @param DdJD       D(xe_old) * dJ(D(xe_old))/dD(xe_old) * D(xe_old)
      @returns          F_pys(xe,xe_old) = - D(x_old)*dJ/dx*D(x_old) : inv(D(x)
   */
   double eval_F_phys(const Matrix& D_inv, const Matrix& DdJD) const;

   /* evaluate F_pys (GENERALIZED)
      @param epsilon
      @param Gamma
      @param D_hat      extended stiffness tensor D(xe)
      @param D_hat_old  extended stiffness tensor D(xe_old)
      @returns          F_pys(xe,xe_old) = - ε ⋅ [ΔD inv(I + Γ ΔD) ε]
   */
   double eval_F_phys_gen(const Vector& epsilon, const Matrix& Gamma, const Matrix& D_hat, const Matrix& D_hat_old) const;

   /* evaluate F_vol
      @param xe         current material configuration
      @param lambda     volume multiplier
      @returns          F_vol(lambda,xe) = lambda * rho_e * v_FE_e / v_tot
   */
   double eval_F_vol(const Material& xe, double v_FE_e, double lambda) const;

   /* evaluate F_reg
      @param xe         current material configuration
      @param dAF_ee     diagonal element dAF(e,e)
      @param dAFR_e     dAFR(e) corresponding to F_REG::ANGLE
      @param dAFR_s_e   dAFR(e) corresponding to F_REG::SIN_COS for the sin(2φ) term
      @param dAFR_c_e   dAFR(e) corresponding to F_REG::SIN_COS for the cos(2φ) term
      @param p_filt     filtering penalty
      @returns          F_reg(xe,xe_old) = p_filt * (0.5*xe*dAF_ee + dAFR_e) * xe
   */
   double eval_F_reg(const Material& xe, const Material& dAF_ee, const Material& dAFR_e, const Material& dAFR_s_e, const Material& dAFR_c_e, const Material& p_filt) const;

   /* evaluate 0.5*tau*||xe-xe_old||^2
      @param xe         current material configuration
      @param xe_old     previous iterate
      @param tau        globalization penalty
      @returns          F_merit(xe,xe_old)
   */
   double eval_F_glob(const Material& xe, const Material& xe_old, double tau) const;

   /* partially precompute inverse stiffness tensors for all possible grid points
   */
   void precompute_tensors(const int n_levels, const Bounds& bounds, const Samplesize& n_samples, double p);

   //===========================================================

   const Matrix _D_core;     // stiffness of core material
   const Matrix _D_core_inv; // inverse stiffness tensor
   const Vector _v_FE;       // finite element volumes
   const double _v_tot;      // sum_i(_v_FE[i])

   F_REG _filt_model; // model for F_reg

   std::vector<std::vector<std::vector<Matrix>>> _D_inv_precomputed;      // precomputed values for (Q * D_core * Q^t)^(-1)
   std::vector<double>                           _pow_rho_mp_precomputed; // precomputed values for rho^(-p)

   mutable Info _info; // status information

   mutable std::array<Timer, Timer::Sections::N_TIMER_SECTIONS> timingTree; // time measurement for algorithm

   //===========================================================

   // for testing purposes
   friend class test::SGP_Testclass;
};

} // namespace sgp
