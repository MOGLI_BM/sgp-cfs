/*
    Copyright (c) 2021 Benjamin Mann

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */
#pragma once

#include <array>
#include <cassert>
#include <initializer_list>
#include <vector>

#include "csr_matrix.hpp"
#include "datatypes.hpp"

namespace sgp {

enum MaterialIndex
{
   PHI,
   THETA,
   PSI,
   RHO
};

struct DesignVariables
{
   static constexpr std::array<MaterialIndex, 4> all3D{{PHI, THETA, PSI, RHO}};
   static constexpr std::array<MaterialIndex, 2> all2D{{PHI, RHO}};
   static constexpr std::array<MaterialIndex, 3> angles3D{{PHI, THETA, PSI}};
   static constexpr std::array<MaterialIndex, 1> angles2D{{PHI}};

   static const std::vector<std::string> name; //{{"phi", "theta", "psi", "rho"}};
};

template <typename T, int N>
struct Material_Parameters : public std::array<T, N>
{
   Material_Parameters() { static_assert(3 <= N && N <= 4, "Not Implemented");
      this->fill(T(0));
   }

   Material_Parameters(std::initializer_list<T> args)
   : Material_Parameters()
   {
      assert(args.size() == this->size());
      std::copy(args.begin(), args.end(), this->data());
   }

   Material_Parameters(const T& _phi, const T& _theta, const T& _psi)
   : Material_Parameters{_phi, _theta, _psi}
   {}

   Material_Parameters(const T& _phi, const T& _theta, const T& _psi, const T& _rho)
   : Material_Parameters{_phi, _theta, _psi, _rho}
   {}
};

using Material            = Material_Parameters<double, 4>;
using Samplesize          = Material_Parameters<int, 4>;
using Rotation            = Material_Parameters<double, 3>;
using Material_Vector     = Material_Parameters<std::vector<double>, 4>;
using Material_Matrix_ptr = Material_Parameters<const LDR_Matrix*, 4>;

struct Bounds
{
   Material min, max;
};

template <typename T, int N>
inline std::ostream& operator<<(std::ostream& out, const Material_Parameters<T, N>& M)
{
   for (int i = 0; i < N; ++i)
   {
      out << M[i] << " ";
   }

   return out;
}

} // namespace sgp
