/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/
#include "tensor.hpp"

#include <cassert>
#include <cstring>
#include <iostream>
#include <mkl.h>

namespace sgp {

void Matrix::setData(const double* dat)
{
   std::memcpy(data(), dat, N * N * sizeof(double));
}

void Matrix::inv()
{
   //todo
   // if (symmetric) // use cholesky
   // {
   //    // cholesky factorization
   //    LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'U', N, data(), N);
   //    // inversion
   //    LAPACKE_dpotri(LAPACK_ROW_MAJOR, 'U', N, data(), N);
   // }
   // else // use LU
   // {
   std::vector<lapack_int> ipiv(N);
   // LU decomposition
   LAPACKE_dgetrf(LAPACK_ROW_MAJOR, N, N, data(), N, ipiv.data());
   // inversion
   LAPACKE_dgetri(LAPACK_ROW_MAJOR, N, data(), N, ipiv.data());
   // }
}

void Matrix::trans()
{
   mkl_dimatcopy('R', 'T', N, N, 1.0, this->data(), N, N);
}

void Matrix::shift_diagonal(double scalar)
{
   for (int i = 0; i < N; ++i)
   {
      (*this)(i, i) += scalar;
   }
}

Vector Matrix::apply(const Vector& x, bool transpose) const
{
   assert(N == int(x.size()));

   Vector Ax(N);

   if (transpose)
   {
      cblas_dgemv(CblasRowMajor, CblasTrans, N, N, 1, this->data(), N, x.data(), 1, 0, Ax.data(), 1);
   }
   else
   {
      cblas_dgemv(CblasRowMajor, CblasNoTrans, N, N, 1, this->data(), N, x.data(), 1, 0, Ax.data(), 1);
   }

   return Ax;
}

Vector Matrix::apply_inverse(const Vector& b) const
{
   assert(N == int(b.size()));

   Vector                  x   = b;
   Matrix                  PLU = *this;
   std::vector<lapack_int> piv(N);

   LAPACKE_dgesv(LAPACK_ROW_MAJOR, N, 1, PLU.data(), N, piv.data(), x.data(), 1);

   return x;
}

Matrix Matrix::operator*(const Matrix& B) const
{
   assert(N == B.N);

   Matrix C(N);

   cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, this->data(), N, B.data(), N, 0.0, C.data(), N);

   return C;
}

Matrix Matrix::operator+(const Matrix& B) const
{
   assert(N == B.N);

   Matrix C(N);

   mkl_domatadd('r', 'n', 'n', N, N, 1.0, this->data(), N, 1.0, B.data(), N, C.data(), N);

   return C;
}

Matrix Matrix::operator-(const Matrix& B) const
{
   assert(N == B.N);

   Matrix C(N);

   mkl_domatadd('r', 'n', 'n', N, N, 1.0, this->data(), N, -1.0, B.data(), N, C.data(), N);

   return C;
}

Matrix& Matrix::syprd(const Matrix& Q, const Matrix& C, Matrix& D, bool reversed)
{
   assert(Q.N == C.N && C.N == D.N);

   auto N = D.rows();

   Matrix tmp(D.N);

   if (reversed)
   {
      cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, N, N, N, 1.0, Q.data(), N, C.data(), N, 0.0, tmp.data(), N);
      cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, tmp.data(), N, Q.data(), N, 0.0, D.data(), N);
   }
   else
   {
      cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, Q.data(), N, C.data(), N, 0.0, tmp.data(), N);
      cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, N, N, N, 1.0, tmp.data(), N, Q.data(), N, 0.0, D.data(), N);
   }

   return D;
}

std::ostream& operator<<(std::ostream& out, const Matrix& M)
{
   out << "[";

   for (int i = 0; i < M.rows(); ++i)
   {
      for (int j = 0; j < M.cols(); ++j)
      {
         out << M(i, j) << " ";
      }

      if (i < M.rows() - 1)
      {
         out << ";\n";
      }
      else
      {
         out << "]\n";
      }
   }

   return out;
}

void Matrix::extend_to_block_matrix(const Matrix& A, Matrix& A_ex, int n)
{
   assert(A.N * n == A_ex.N);

   for (int i = 0; i < A_ex.N; ++i)
   {
      int k  = i / A.N;
      int ii = i % A.N;

      for (int j = 0; j < A_ex.N; ++j)
      {
         A_ex(i, j) = (j / A.N == k) ? A(ii, j % A.N) : 0;
      }
   }
}

} // namespace sgp
