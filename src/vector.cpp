/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "vector.hpp"

#include <cassert>
#include <mkl.h>

namespace sgp {

double dot(const Vector& x, const Vector& y)
{
   assert(x.size() == y.size());
   return cblas_ddot(x.size(), x.data(), 1, y.data(), 1);
}

Vector scale(double a, const Vector& x)
{
   Vector ax = x;
   cblas_dscal(x.size(), a, ax.data(), 1);
   return ax;
}

Vector sum(const Vector& x, const Vector& y)
{
   Vector z(x.size()); // x+y;
   vdAdd(x.size(), x.data(), y.data(), z.data());
   return z;
}

Vector diff(const Vector& x, const Vector& y)
{
   Vector minus_y = scale(-1, y); // -y
   Vector z(x.size());            // x-y;

   vdAdd(x.size(), x.data(), minus_y.data(), z.data());
   return z;
}

} // namespace sgp
