/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/
#pragma once

#include "csr_matrix.hpp"
#include "datatypes.hpp"
#include "material_parameters.hpp"
#include "tensor.hpp"

namespace sgp {

// struct for storing sgp parameters required for initialization
struct InitParams
{
   // spacial dimension of simulation domain (must be either 2 or 3)
   int dim;
   // stiffness tensor of core material
   Matrix D_core;
   // finite element volumes
   Vector v_FE;
};

// struct for storing sgp parameters required for solving the optimization problem
struct SolveParams
{
   // volume bound
   double v_star;
   // power law param
   double p;
   // max number of iterations for outer loop and volume bisection loop
   int iter_max, iter_bisec_max;
   // stopping criterion for energy minimum and volume bisection
   double J_stop, eps;
   /* levels and samplesize per level defining resolution of parameter space
      note: phi   = rotation about x-axis (z-axis in 2D)
            theta = rotation about y-axis (unused in 2D)
            psi   = rotation about z-axis (unused in 2D)
   */
   int n_levels, n_rho, n_phi, n_theta, n_psi;
   // precompute stiffness tensors for each point in the discretized parameter space
   bool precompute_tensors;
   /* upper and lower bound for parameters rho,phi,theta,psi
      note: phi   = rotation about x-axis (z-axis in 2D)
            theta = rotation about y-axis (unused in 2D)
            psi   = rotation about z-axis (unused in 2D)
   */
   double rhoMin, rhoMax, phiMin, phiMax, thetaMin, thetaMax, psiMin, psiMax;
   // filtering penalty for density and angle parameters
   double p_filt_rho, p_filt_phi;
   // globalization penalty
   double tau;
   // multiplicative increment for globalization penalty
   double tau_incr;
   // minimum and maximum value for volume multiplier
   double lambda_min, lambda_max;
   // print information to console after each n_th iteration; info=0 -> don't show any info
   int info;
   // name of the file the output shall be written to. If left empty, stdout will be used.
   std::string outputfile;
   // inital material configuration
   Vector rho0;   // initial density
   Vector phi0;   // initial rotation about the x-axis (z-axis in 2D)
   Vector theta0; // initial rotation about the y-axis (unused in 2D)
   Vector psi0;   // initial rotation about the z-axis (unused in 2D)
   // filter matrices
   CSR_Matrix F_rho;
   CSR_Matrix F_phi;
   // model for F_phys
   F_PHYS model;
   // type of filter term
   F_REG filt;
   // number of integration points per element
   int n_ip;
};

} // namespace sgp
