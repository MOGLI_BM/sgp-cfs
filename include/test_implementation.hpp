/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include "sgp.hpp"

namespace sgp {
namespace test {

/* derived sgp class for testing interior functionality
*/
class SGP_Testclass : public sgp::Algorithm
{
   using sgp::Algorithm::Algorithm;

   void   eval_Gamma_epsilon(const sgp::Material_Vector& x,
                             std::vector<sgp::Matrix>&   Gamma,
                             std::vector<sgp::Vector>&   epsilon) const override;
   void   eval_dJ(const sgp::Material_Vector& x, std::vector<sgp::Matrix>& dJ) const override;
   double eval_J(const sgp::Material_Vector& x) const override;

 public:
   LDR_Matrix get_dAF(const CSR_Matrix& F) const { return compute_dAF(F); }
};

InitParams  sample_params_init(int dim, int n_el);
SolveParams sample_params_solve(int dim, int n_el);

} // namespace test
} // namespace sgp