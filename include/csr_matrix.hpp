/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#pragma once

#include <iostream>
#include <mkl.h>

#include "datatypes.hpp"
#include "vector.hpp"

namespace sgp {

// wrapper for mkl-sparse-matrix
class CSR_Matrix
{
 public:
   // create diagonal matrix from vector of diagonal entries
   static CSR_Matrix diag(const std::vector<double>& diag_val);

   // create identity matrix of dimension nxn
   static CSR_Matrix id(int n);

   // Ctor for identity matrix of dimension nxn
   CSR_Matrix(int n = 0);

   /* create MKL sparse CSR matrix from input data
      @params rows            number of matrix rows
      @params cols            number of matrix columns
      @params row_start_idx   index-array, s.th. values[j] with j = row_start_idx[i]
                              is the first non-zero entry in row i.
                              Note: row_start_idx contains rows elements
      @params row_end_idx     index-array, s.th. values[j-1] with j = row_end_idx[i]
                              is the last non-zero entry in row i
                              Note: row_start_idx contains rows elements
      @params col_idx         index-array, s.th. col_idx[k] is the column where values[k]
                              is located.
      @params values          array of non-zero entries of the matrix
    */
   template <typename INT_T>
   CSR_Matrix(int           rows,
              int           cols,
              const INT_T*  row_start_idx,
              const INT_T*  row_end_idx,
              const INT_T*  col_idx,
              const double* values);

   /* create MKL sparse CSR matrix from input data
      @params rows            number of matrix rows
      @params cols            number of matrix columns
      @params row_idx         index-array, s.th. values[j] and values[k-1] with j = row_idx[i]
                              and k = row_idx[i+1] are the firs and last non-zero entries
                              in row i, respectively.
                              Note: row_idx contains rows+1 elements
      @params col_idx         index-array, s.th. col_idx[k] is the column where values[k]
                              is located
      @params values          array of non-zero entries of the matrix
    */
   template <typename INT_T>
   CSR_Matrix(int rows, int cols, const INT_T* row_idx, const INT_T* col_idx, const double* values);

   /* wrap mkl-sparse-matrix handle for convenient access
      @params mkl_handle  sparse-matrix-handle
      @params copy        if true, the matrix data in mkl_handle will be copied.
                           if false, No data will be copied! Furthermore,
                           the destructor will call mkl_sparse_destroy(mkl_handle).
                           Do not attempt to destroy mkl_handle yourself!
                           USE WITH CAUTION!
    */
   CSR_Matrix(sparse_matrix_t mkl_handle, bool copy);

   CSR_Matrix(const CSR_Matrix& other);
   CSR_Matrix(CSR_Matrix&& other);

   ~CSR_Matrix();

   inline bool operator!() const { return (_rows == 0 or _cols == 0); }

   CSR_Matrix& operator=(const CSR_Matrix& other);
   CSR_Matrix& operator=(CSR_Matrix&& other);

   CSR_Matrix operator*(const CSR_Matrix& B) const;
   CSR_Matrix operator-(const CSR_Matrix& B) const;
   CSR_Matrix operator+(const CSR_Matrix& B) const;

   // find ij entry of matrix !!DO NOT USE IN PERFORMANCE CRITICAL SECTIONS!!
   double operator()(const int i, const int j) const;

   // compute y = αAx + βy
   Vector& apply(const Vector& x, Vector& y, double alpha = 1, double beta = 0) const;

   // compute A^T * A
   CSR_Matrix AtA() const;

   /* @returns vector containing the diagonal entries
   */
   Vector diag() const;

   // return Frobenius norm of the matrix
   double normF() const;

   // return transpose of the matrix; !!DO NOT USE IN PERFORMANCE CRITICAL SECTIONS!!
   CSR_Matrix transpose() const;

   int rows() const { return _rows; }
   int cols() const { return _cols; }
   int nonzeros() const { return _row_end[_rows - 1]; }

   friend std::ostream& operator<<(std::ostream& out, const CSR_Matrix& M);

   friend class LDR_Matrix;

 private:
   // copy data from mkl_handle to new internal variable _mkl_handle
   void copy_mkl_sparse_matrix(const sparse_matrix_t mkl_handle);
   // obtain _rows, _cols, _row_start, _row_end, _col_idx and _variables from _mkl_handle
   void get_data_from_handle();
   // helper for operator=()
   void assign(sparse_matrix_t mkl_handle, bool copy);

   sparse_matrix_t _mkl_handle;

   MKL_INT _rows;
   MKL_INT _cols;

   MKL_INT* _row_start;
   MKL_INT* _row_end;
   MKL_INT* _col_idx;
   double*  _values;
};

/* CSR-matrix, stored as A = L+D+R with
   * diagonal matrix D
   * strict lower triangular matrix L
   * strict upper triangular matrix R
*/
struct LDR_Matrix
{
   LDR_Matrix() {}

   LDR_Matrix(const CSR_Matrix& A)
   : LR(A.rows())
   , D(A.diag())
   {
      // LR <- -D
      for (int i = 0; i < int(D.size()); ++i)
      {
         LR._values[i] = -D[i];
      }
      // LR <- -D + A = L+R
      LR = LR + A;
   }

   inline bool operator!() const { return !LR; }

   CSR_Matrix LR; // L+R = A-D
   Vector     D;  // diag(A)
};

} // namespace sgp
