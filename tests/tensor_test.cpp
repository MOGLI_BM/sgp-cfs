/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "random.hpp"
#include "tensor.hpp"
#include "testing.hpp"
#include "vector.hpp"

using namespace sgp;
using namespace test;

void fill(Matrix& D, Random& random, bool sym = false)
{
   for (int i = 0; i < D.rows(); ++i)
   {
      for (int j = i; j < D.cols(); ++j)
      {
         D(i, j) = random.real();
         D(j, i) = (sym) ? D(i, j) : random.real();
      }
   }
}

Vector create_random_vector(int n, Random& random)
{
   Vector vec(n);

   for (int i = 0; i < n; ++i)
   {
      vec[i] = random.real();
   }

   return vec;
}

void syprd_test(const Matrix& Q, const Matrix& C)
{
   std::cout << "TEST: Matrix::syprd(Q,C,QCQt)\n";

   Matrix Qt(Q); // Q^t
   Qt.trans();

   Matrix QCQ(Q.rows()); // result

   Matrix::syprd(Q, C, QCQ, false);
   auto        err = diff(QCQ, Q * C * Qt);
   std::string msg = "syprd(Q,C, reversed=false) != Q*C*Q^t";
   check_almost_zero(err, msg, 1e-12);

   Matrix::syprd(Q, C, QCQ, true);
   err = diff(QCQ, Qt * C * Q);
   msg = "syprd(Q,C, reversed=true) != Q^t*C*Q";
   check_almost_zero(err, msg, 1e-12);
}

void matmul_test(const Matrix& A, const Matrix& B, const Vector& x)
{
   std::cout << "TEST: Matrix::operator*(B)\n";

   auto        Bx  = B.apply(x);
   auto        ABx = A.apply(Bx);
   auto        err = diff(ABx, (A * B).apply(x));
   std::string msg = "(A*B)*x != A*(B*x)";
   check_almost_zero(err, msg, 1e-12);
}

void inverse_test(const Matrix& A)
{
   std::cout << "TEST: Matrix::inv()\n";

   auto Ai = A; // A^-1
   Ai.inv();

   Matrix I(A.rows()); // id
   for (int i = 0; i < I.rows(); ++i)
   {
      I(i, i) = 1;
   }

   auto        err = diff(A * Ai, I);
   std::string msg = "A*A^-1 != I";
   check_almost_zero(err, msg, 1e-13);
}

void apply_inverse_test(const Matrix& A, const Vector& b)
{
   std::cout << "TEST: Matrix::apply_inverse(b)\n";

   auto        x      = A.apply_inverse(b);
   auto        AinvAb = A.apply(x);
   auto        err    = diff(b, A.apply(x));
   std::string msg    = "A*x != b";
   check_almost_zero(err, msg, 1e-10);
}

void transpose_test(const Matrix& A)
{
   std::cout << "TEST: Matrix::trans()\n";

   auto At = A; // A^t
   At.trans();

   Matrix At_manual(A.rows()); // A^t (manual)
   for (int i = 0; i < A.rows(); ++i)
   {
      for (int j = 0; j < A.cols(); ++j)
      {
         At_manual(j, i) = A(i, j);
      }
   }

   auto        err = diff(At, At_manual);
   std::string msg = "[A^T]_ji != A_ij";
   check_almost_zero(err, msg, 1e-13);
}

int main(int argc, char const* argv[])
{
   // random engine
   const int n_cases = 50;
   Random       random(-10, 10, 17);

   // run tests
   for (int dim = 2; dim <= 3; ++dim)
   {
      for (int i = 0; i < n_cases; ++i)
      {
         // fill tensors
         Matrix Q(n_VoigtNotation(dim)), C(n_VoigtNotation(dim)), B(n_VoigtNotation(dim));
         Matrix M_hat(n_VoigtNotation(dim) * 4);
         fill(Q, random);
         fill(C, random, true);
         fill(B, random);
         fill(M_hat, random);

         auto x = create_random_vector(Q.rows(), random);
         auto b = create_random_vector(M_hat.rows(), random);

         // std::cout << dim << "D tensors: \n Q = \n" << Q << ", C = \n" << C;

         // symmetric product
         syprd_test(Q, C);

         // A*B
         matmul_test(Q, B, x);

         // A.inv()
         inverse_test(Q);

         // A.trans()
         transpose_test(Q);

         // A.apply_inv()
         apply_inverse_test(M_hat, b);

         // todo more tests
      }
   }

   return 0;
}
