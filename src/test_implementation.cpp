/*
   Copyright (c) 2021 Benjamin Mann

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

#include "test_implementation.hpp"

#include <numeric>

namespace sgp {
namespace test {

//! sample implementation without physical meaning -- just for testing purposes
void SGP_Testclass::eval_Gamma_epsilon(const sgp::Material_Vector&, std::vector<sgp::Matrix>&, std::vector<sgp::Vector>&) const {}

//! sample implementation without physical meaning -- just for testing purposes
void SGP_Testclass::eval_dJ(const sgp::Material_Vector& x, std::vector<sgp::Matrix>& dJ) const
{
   for (int e = 0; e < _n_el; ++e)
   {
      // set dJ_e = rho_e * I
      for (int i = 0; i < dJ[e].rows(); ++i)
      {
         for (int j = 0; j < dJ[e].cols(); ++j)
         {
            dJ[e](i, j) = (i == j) ? x[sgp::RHO][e] : 0.0;
         }
      }
   }
}

//! sample implementation without physical meaning -- just for testing purposes
double SGP_Testclass::eval_J(const sgp::Material_Vector& x) const
{
   // return sum_e(rho_e)
   return std::accumulate(x[sgp::RHO].begin(), x[sgp::RHO].end(), 0.0);
}

//! sample init parameters for algorithm -- just for testing purposes
InitParams sample_params_init(int dim, int n_el)
{
   InitParams initargs;

   initargs.dim = dim;

   initargs.D_core = Matrix(sgp::n_VoigtNotation(dim)); // stiffness of core material
   for (int i = 0; i < initargs.D_core.rows(); ++i)
   {
      initargs.D_core(i, i) = 1;
   }
   initargs.v_FE = sgp::Vector(n_el, 1.0); // element volumes

   return initargs;
}

//! sample solve parameters for algorithm -- just for testing purposes
SolveParams sample_params_solve(int dim, int n_el)
{
   constexpr double pi = 3.14159265358979323846264;

   SolveParams solveargs;

   solveargs.model = sgp::F_PHYS::SIMPLIFIED;
   solveargs.n_ip  = 3;

   solveargs.info           = 1;     // print info in each iteration
   solveargs.iter_max       = 3;     // maximum number of outer iterations
   solveargs.iter_bisec_max = 10;    // maximum number of inner iterations
   solveargs.J_stop         = 1e-10; // stopping criterion for energy minimum
   solveargs.eps            = 1e-10; // stopping criterion for volume bisection
   solveargs.p_filt_rho     = 1.0;   // filter penalty
   solveargs.p_filt_phi     = 1.0;   // filter penalty
   solveargs.tau            = 0.5;   // globalization penalty
   solveargs.tau_incr       = 1.1;   // increment for tau

   solveargs.v_star = 1.0;                   // volume bound
   solveargs.F_rho  = sgp::CSR_Matrix(n_el); // filter matrix
   solveargs.F_phi  = sgp::CSR_Matrix(n_el); // filter matrix
   solveargs.p      = 1.0;                   // power law parameter

   solveargs.n_levels = 3; // number of refinement levels
   solveargs.n_rho    = 5; // number of samples per grid level
   solveargs.n_phi    = 5; // number of samples per grid level
   solveargs.n_theta  = 5; // number of samples per grid level
   solveargs.n_psi    = 5; // number of samples per grid level

   solveargs.rhoMin = 0.1;                    // minimum parameter value
   solveargs.rhoMax = 1.0;                    // maximum parameter value
   solveargs.phiMin = 0;                      // minimum parameter value
   solveargs.phiMax = pi;                     // maximum parameter value
   solveargs.rho0   = sgp::Vector(n_el, 1.0); // initial material configuration
   solveargs.phi0   = sgp::Vector(n_el, 1.0); // initial material configuration
   if (dim > 2)
   {
      solveargs.thetaMin = 0;                      // minimum parameter value
      solveargs.thetaMax = pi;                     // maximum parameter value
      solveargs.psiMin   = 0;                      // minimum parameter value
      solveargs.psiMax   = pi;                     // maximum parameter value
      solveargs.theta0   = sgp::Vector(n_el, 1.0); // initial material configuration
      solveargs.psi0     = sgp::Vector(n_el, 1.0); // initial material configuration
   }

   return solveargs;
}

} // namespace test
} // namespace sgp
